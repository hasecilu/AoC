/*
 * Advent of Code 2015
 * --- Day 1: Not Quite Lisp ---
 * https://adventofcode.com/2015/day/1
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

int main()
{
	FILE *file;
	char ch;
	signed int floor = 0;
	unsigned int pos = 1;

	file = fopen("data", "r");
	do {
		ch = fgetc(file);
		if (ch == '(')
			floor++;
		else if (ch == ')')
			floor--;
		if (floor == -1)
			printf("Position: %d\n", pos);
		pos++;
	} while (ch != EOF);
	fclose(file);

	printf("\nFloor: %d\n", floor);

	return 0;
}
