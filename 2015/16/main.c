/*
 * Advent of Code 2015
 * --- Day 16: Aunt Sue ---
 * https://adventofcode.com/2015/day/16
 * --------------------
 * Author: @hasecilu
 * 
 * Preprocess ticker_tape file to get all unique keys (items) and the perfect minimal hash function
 * 	awk '{print $1}' ticker_tape | sed 's/://' > keys && triehash keys > triehash.h
 */

#include <stdio.h>
#include "triehash.h"

#define BUFF_SIZE	52
#define AUNTS	 	500
#define ITEMS		10
#define CHILDREN 	3
#define CATS 		7
#define SAMOYEDS 	2
#define POMERANIAS 	3
#define AKITAS 		0
#define VIZSLAS 	0
#define GOLDFISH 	5
#define TREES 		3
#define CARS 		2
#define PERFUMES 	1

int sues[AUNTS][ITEMS], i;

void add_sue(char *buffer);
void sue_finder();
int slen(char *s);

int main()
{
	for (size_t i = 0; i < AUNTS; i++)
		for (size_t j = 0; j < ITEMS; j++)
			sues[i][j] = -1;

	FILE *file;
	char buffer[BUFF_SIZE];

	file = fopen("input", "r");
	while (fgets(buffer, sizeof(buffer), file) != NULL) {
		add_sue(buffer);
	}
	fclose(file);

	sue_finder();

	return 0;
}

/* Parse data and write it to matrix. */
void add_sue(char *buffer)
{
	unsigned int x1, x2, x3;
	char item1[12], item2[12], item3[12];
	if (sscanf
	    (buffer, "Sue %*u: %[^:]: %u, %[^:]: %u, %[^:]: %u", item1, &x1,
	     item2, &x2, item3, &x3) == 6) {
		/* printf("%u: %s %u %s %u %s %u\n", i, item1, x1, item2, x2, item3, x3); */
		sues[i][PerfectHash(item1, slen(item1))] = x1;
		sues[i][PerfectHash(item2, slen(item2))] = x2;
		sues[i][PerfectHash(item3, slen(item3))] = x3;
		i++;
	}

}

/* Find the correct aunt Sue, all true until the contrary is demostrated. */
void sue_finder()
{
	for (size_t i = 0; i < AUNTS; i++) {
		unsigned int sue_letter = 1;
		if (sues[i][0] != -1 && sues[i][0] != CHILDREN)
			sue_letter = 0;
		if (sue_letter && sues[i][1] != -1 && sues[i][1] <= CATS)
			sue_letter = 0;
		if (sue_letter && sues[i][2] != -1 && sues[i][2] != SAMOYEDS)
			sue_letter = 0;
		if (sue_letter && sues[i][3] != -1 && sues[i][3] >= POMERANIAS)
			sue_letter = 0;
		if (sue_letter && sues[i][4] != -1 && sues[i][4] != AKITAS)
			sue_letter = 0;
		if (sue_letter && sues[i][5] != -1 && sues[i][5] != VIZSLAS)
			sue_letter = 0;
		if (sue_letter && sues[i][6] != -1 && sues[i][6] >= GOLDFISH)
			sue_letter = 0;
		if (sue_letter && sues[i][7] != -1 && sues[i][7] <= TREES)
			sue_letter = 0;
		if (sue_letter && sues[i][8] != -1 && sues[i][8] != CARS)
			sue_letter = 0;
		if (sue_letter && sues[i][9] != -1 && sues[i][9] != PERFUMES)
			sue_letter = 0;
		if (sue_letter && sue_letter)
			printf("%lu\n", i + 1);
	}
}

/* Return string length */
int slen(char *s)
{
	int i;
	for (i = 0; s[i] != '\0'; ++i) ;
	return i;
}
