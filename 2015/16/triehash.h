#ifndef TRIE_HASH_PerfectHash
#define TRIE_HASH_PerfectHash
#include <stddef.h>
#include <stdint.h>
enum PerfectKey {
    children = 0,
    cats = 1,
    samoyeds = 2,
    pomeranians = 3,
    akitas = 4,
    vizslas = 5,
    goldfish = 6,
    trees = 7,
    cars = 8,
    perfumes = 9,
    Unknown = -1,
};
static enum PerfectKey PerfectHash(const char *string, size_t length);
#ifdef __GNUC__
typedef uint16_t __attribute__((aligned (1))) triehash_uu16;
typedef char static_assert16[__alignof__(triehash_uu16) == 1 ? 1 : -1];
typedef uint32_t __attribute__((aligned (1))) triehash_uu32;
typedef char static_assert32[__alignof__(triehash_uu32) == 1 ? 1 : -1];
typedef uint64_t __attribute__((aligned (1))) triehash_uu64;
typedef char static_assert64[__alignof__(triehash_uu64) == 1 ? 1 : -1];
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define onechar(c, s, l) (((uint64_t)(c)) << (s))
#else
#define onechar(c, s, l) (((uint64_t)(c)) << (l-8-s))
#endif
#if (!defined(__ARM_ARCH) || defined(__ARM_FEATURE_UNALIGNED)) && !defined(TRIE_HASH_NO_MULTI_BYTE)
#define TRIE_HASH_MULTI_BYTE
#endif
#endif /*GNUC */
#ifdef TRIE_HASH_MULTI_BYTE
static enum PerfectKey PerfectHash4(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('c', 0, 32)| onechar('a', 8, 32)| onechar('r', 16, 32)| onechar('s', 24, 32):
        return cars;
        break;
    case 0| onechar('c', 0, 32)| onechar('a', 8, 32)| onechar('t', 16, 32)| onechar('s', 24, 32):
        return cats;
    }
    return Unknown;
}
static enum PerfectKey PerfectHash5(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('t', 0, 32)| onechar('r', 8, 32)| onechar('e', 16, 32)| onechar('e', 24, 32):
        switch(string[4]) {
        case 0| onechar('s', 0, 8):
            return trees;
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash6(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('a', 0, 32)| onechar('k', 8, 32)| onechar('i', 16, 32)| onechar('t', 24, 32):
        switch(string[4]) {
        case 0| onechar('a', 0, 8):
            switch(string[5]) {
            case 0| onechar('s', 0, 8):
                return akitas;
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash7(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('v', 0, 32)| onechar('i', 8, 32)| onechar('z', 16, 32)| onechar('s', 24, 32):
        switch(string[4]) {
        case 0| onechar('l', 0, 8):
            switch(string[5]) {
            case 0| onechar('a', 0, 8):
                switch(string[6]) {
                case 0| onechar('s', 0, 8):
                    return vizslas;
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash8(const char *string)
{
    switch(*((triehash_uu64*) &string[0])) {
    case 0| onechar('c', 0, 64)| onechar('h', 8, 64)| onechar('i', 16, 64)| onechar('l', 24, 64)| onechar('d', 32, 64)| onechar('r', 40, 64)| onechar('e', 48, 64)| onechar('n', 56, 64):
        return children;
        break;
    case 0| onechar('g', 0, 64)| onechar('o', 8, 64)| onechar('l', 16, 64)| onechar('d', 24, 64)| onechar('f', 32, 64)| onechar('i', 40, 64)| onechar('s', 48, 64)| onechar('h', 56, 64):
        return goldfish;
        break;
    case 0| onechar('p', 0, 64)| onechar('e', 8, 64)| onechar('r', 16, 64)| onechar('f', 24, 64)| onechar('u', 32, 64)| onechar('m', 40, 64)| onechar('e', 48, 64)| onechar('s', 56, 64):
        return perfumes;
        break;
    case 0| onechar('s', 0, 64)| onechar('a', 8, 64)| onechar('m', 16, 64)| onechar('o', 24, 64)| onechar('y', 32, 64)| onechar('e', 40, 64)| onechar('d', 48, 64)| onechar('s', 56, 64):
        return samoyeds;
    }
    return Unknown;
}
static enum PerfectKey PerfectHash11(const char *string)
{
    switch(*((triehash_uu64*) &string[0])) {
    case 0| onechar('p', 0, 64)| onechar('o', 8, 64)| onechar('m', 16, 64)| onechar('e', 24, 64)| onechar('r', 32, 64)| onechar('a', 40, 64)| onechar('n', 48, 64)| onechar('i', 56, 64):
        switch(string[8]) {
        case 0| onechar('a', 0, 8):
            switch(string[9]) {
            case 0| onechar('n', 0, 8):
                switch(string[10]) {
                case 0| onechar('s', 0, 8):
                    return pomeranians;
                }
            }
        }
    }
    return Unknown;
}
#else
static enum PerfectKey PerfectHash4(const char *string)
{
    switch(string[0]) {
    case 'c':
        switch(string[1]) {
        case 'a':
            switch(string[2]) {
            case 'r':
                switch(string[3]) {
                case 's':
                    return cars;
                }
                break;
            case 't':
                switch(string[3]) {
                case 's':
                    return cats;
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash5(const char *string)
{
    switch(string[0]) {
    case 't':
        switch(string[1]) {
        case 'r':
            switch(string[2]) {
            case 'e':
                switch(string[3]) {
                case 'e':
                    switch(string[4]) {
                    case 's':
                        return trees;
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash6(const char *string)
{
    switch(string[0]) {
    case 'a':
        switch(string[1]) {
        case 'k':
            switch(string[2]) {
            case 'i':
                switch(string[3]) {
                case 't':
                    switch(string[4]) {
                    case 'a':
                        switch(string[5]) {
                        case 's':
                            return akitas;
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash7(const char *string)
{
    switch(string[0]) {
    case 'v':
        switch(string[1]) {
        case 'i':
            switch(string[2]) {
            case 'z':
                switch(string[3]) {
                case 's':
                    switch(string[4]) {
                    case 'l':
                        switch(string[5]) {
                        case 'a':
                            switch(string[6]) {
                            case 's':
                                return vizslas;
                            }
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash8(const char *string)
{
    switch(string[0]) {
    case 'c':
        switch(string[1]) {
        case 'h':
            switch(string[2]) {
            case 'i':
                switch(string[3]) {
                case 'l':
                    switch(string[4]) {
                    case 'd':
                        switch(string[5]) {
                        case 'r':
                            switch(string[6]) {
                            case 'e':
                                switch(string[7]) {
                                case 'n':
                                    return children;
                                }
                            }
                        }
                    }
                }
            }
        }
        break;
    case 'g':
        switch(string[1]) {
        case 'o':
            switch(string[2]) {
            case 'l':
                switch(string[3]) {
                case 'd':
                    switch(string[4]) {
                    case 'f':
                        switch(string[5]) {
                        case 'i':
                            switch(string[6]) {
                            case 's':
                                switch(string[7]) {
                                case 'h':
                                    return goldfish;
                                }
                            }
                        }
                    }
                }
            }
        }
        break;
    case 'p':
        switch(string[1]) {
        case 'e':
            switch(string[2]) {
            case 'r':
                switch(string[3]) {
                case 'f':
                    switch(string[4]) {
                    case 'u':
                        switch(string[5]) {
                        case 'm':
                            switch(string[6]) {
                            case 'e':
                                switch(string[7]) {
                                case 's':
                                    return perfumes;
                                }
                            }
                        }
                    }
                }
            }
        }
        break;
    case 's':
        switch(string[1]) {
        case 'a':
            switch(string[2]) {
            case 'm':
                switch(string[3]) {
                case 'o':
                    switch(string[4]) {
                    case 'y':
                        switch(string[5]) {
                        case 'e':
                            switch(string[6]) {
                            case 'd':
                                switch(string[7]) {
                                case 's':
                                    return samoyeds;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash11(const char *string)
{
    switch(string[0]) {
    case 'p':
        switch(string[1]) {
        case 'o':
            switch(string[2]) {
            case 'm':
                switch(string[3]) {
                case 'e':
                    switch(string[4]) {
                    case 'r':
                        switch(string[5]) {
                        case 'a':
                            switch(string[6]) {
                            case 'n':
                                switch(string[7]) {
                                case 'i':
                                    switch(string[8]) {
                                    case 'a':
                                        switch(string[9]) {
                                        case 'n':
                                            switch(string[10]) {
                                            case 's':
                                                return pomeranians;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
#endif /* TRIE_HASH_MULTI_BYTE */
static enum PerfectKey PerfectHash(const char *string, size_t length)
{
    switch (length) {
    case 4:
        return PerfectHash4(string);
    case 5:
        return PerfectHash5(string);
    case 6:
        return PerfectHash6(string);
    case 7:
        return PerfectHash7(string);
    case 8:
        return PerfectHash8(string);
    case 11:
        return PerfectHash11(string);
    default:
        return Unknown;
    }
}
#endif                       /* TRIE_HASH_PerfectHash */
