/*
 * Advent of Code 2015
 * --- Day 5: Doesn't He Have Intern-Elves For This? ---
 * https://adventofcode.com/2015/day/5
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <string.h>

#define	BUFF_SIZE	18
#define PART		1

int main()
{
	FILE *p;
	char buff[BUFF_SIZE];
	int nice = 0;

	p = fopen("data", "r");

	while (fgets(buff, BUFF_SIZE, p) != NULL) {
		char len = strlen(buff);
#if PART == 1
		char vows = 0;
		char twice = 0;
		char naugthyS = 0;
		char vowels[5] = { 'a', 'e', 'i', 'o', 'u' };
		char naugthyStrings[2][4] = { {'a', 'c', 'p', 'x'}, {'b', 'd', 'q', 'y'} };

		for (unsigned char i = 0; i < len; i++) {
			for (unsigned char v = 0; v < 5; v++) {
				if (buff[i] == vowels[v])
					vows++;
				else {
					if (buff[i] == buff[i + 1])
						twice++;

					for (unsigned char ns = 0; ns < 4; ns++)
						if (buff[i] == naugthyStrings[0][ns]
						    && buff[i + 1] == naugthyStrings[1][ns])
							naugthyS++;
				}
			}
		}
		if (!(vows < 3 || twice == 0 || naugthyS != 0))
			nice++;
#elif PART == 2
		char pair_twice = 0;
		char sandwich = 0;
		char pair[2];

		for (unsigned char i = 0; i < len; i++) {
			pair[0] = buff[i];
			pair[1] = buff[i + 1];
			for (unsigned char j = i + 2; j < len; j++) {
				if (buff[j] == pair[0]
				    && buff[j + 1] == pair[1])
					pair_twice++;
			}

			if (i <= len - 2 - 1)
				if (buff[i] == buff[i + 2])
					sandwich++;
		}
		if (pair_twice > 0 && sandwich > 0)
			nice++;
#endif
	}
	fclose(p);

	printf("\n%d nice strings\n", nice);

	return 0;
}
