/*
 * Advent of Code 2015
 * --- Day 4: The Ideal Stocking Stuffer ---
 * https://adventofcode.com/2015/day/4
 * --------------------
 * Author: @hasecilu
 * 
 * Compile command: gcc main.c -lssl -lcrypto
 */

#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

#define SECRET_KEY "ckczppom"

void bytes2md5(const char *data, int len, char *md5buf);

int main()
{
	char md5[33]; // 32 characters + null terminator
	char flag = 0;
	int number = 0;

	while (flag == 0) {
		char s1[100] = SECRET_KEY;

		int length = snprintf(NULL, 0, "%d", number);
		char *s2 = malloc(length + 1);
		snprintf(s2, length + 1, "%d", number);

		strcat(s1, s2);
		//printf("%s\t%s\n", s1, s2);

		bytes2md5(s1, strlen(s1), md5);

		if (md5[0] == '0' && md5[1] == '0' && md5[2] == '0' && md5[3] == '0' &&
		    md5[4] == '0') {
			flag = 1;
			printf("md5sum(%s) = %s\n", s1, md5);
		}

		free(s2);
		number++;
	}

	return 0;
}

// Snippet code by devtty1er@stackoverflow
// https://stackoverflow.com/questions/7627723/how-to-create-a-md5-hash-of-a-string-in-c
void bytes2md5(const char *data, int len, char *md5buf)
{
	// Based on https://www.openssl.org/docs/manmaster/man3/EVP_DigestUpdate.html
	EVP_MD_CTX *mdctx = EVP_MD_CTX_new();
	const EVP_MD *md = EVP_md5();
	unsigned char md_value[EVP_MAX_MD_SIZE];
	unsigned int md_len, i;
	EVP_DigestInit_ex(mdctx, md, NULL);
	EVP_DigestUpdate(mdctx, data, len);
	EVP_DigestFinal_ex(mdctx, md_value, &md_len);
	EVP_MD_CTX_free(mdctx);
	for (i = 0; i < md_len; i++)
		snprintf(&(md5buf[i * 2]), 16 * 2, "%02x", md_value[i]);
}
