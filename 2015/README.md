# Advent of Code 2015

| Day | Problem                                      | Part 1 | Part 2 |
| :-: | :------------------------------------------- | :----: | :----: |
| 01  | [Not Quite Lisp][01]                         |   ✔️    |   ✔️    |
| 02  | [I Was Told There Would Be No Math][02]      |   ✔️    |   ✔️    |
| 03  | [Perfectly Spherical Houses in a Vacuum][03] |   ✔️    |   ✔️    |
| 04  | [The Ideal Stocking Stuffer][04]             |   ✔️    |   ✔️    |
| 05  | [Doesn't He Have Intern-Elves For This?][05] |   ✔️    |   ✔️    |
| 06  | [Probably a Fire Hazard][06]                 |   ✔️    |   ✔️    |
| 07  | [Some Assembly Required][07]                 |   ✔️    |   ✔️    |
| 08  | [Matchsticks][08]                            |   ✔️    |   ✔️    |
| 09  | [All in a Single Night][09]                  |   ✔️    |   ✔️    |
| 10  | [Elves Look, Elves Say][10]                  |   ✔️    |   ✔️    |
| 11  | [Corporate Policy][11]                       |   ✔️    |   ✔️    |
| 12  | [JSAbacusFramework.io][12]                   |   ✔️    |   ❌    |
| 13  | [Knights of the Dinner Table][13]            |   ✔️    |   ✔️    |
| 14  | [Reindeer Olympics][14]                      |   ✔️    |   ✔️    |
| 15  | [Science for Hungry People][15]              |   ✔️    |   ✔️    |
| 16  | [Aunt Sue][16]                               |   ✔️    |   ✔️    |
| 17  | [No Such Thing as Too Much][17]              |   ✔️    |   ✔️    |
| 18  | [Like a GIF For Your Yard][18]               |   ✔️    |   ✔️    |
| 19  | [Medicine for Rudolph][19]                   |   ✔️    |   ❌    |
| 20  | [Infinite Elves and Infinite Houses][20]     |   ✔️    |   ✔️    |
| 21  | [RPG Simulator 20XX][21]                     |   ✔️    |   ✔️    |
| 22  | [Wizard Simulator 20XX][22]                  |   ❌    |   ❌    |
| 23  | [Opening the Turing Lock][23]                |   ❌    |   ❌    |
| 24  | [It Hangs in the Balance][24]                |   ❌    |   ❌    |
| 25  | [Let It Snow][25]                            |   ❌    |   ❌    |

## Makefile

This `Makefile` is used to compile all codes, if there is an exception, a
custom `Makefile` will be available in the corresponding directory.

```Makefile
CC=gcc
CFLAGS=-g -Wall
OBJS=main.o
BIN=main

all: $(BIN)

main: $(OBJS)
        $(CC) $(CFLAGS) $(OBJS) -o main

%.o: %.c
        $(CC) $(CFLAGS) -c $< -o $@

clean:
        $(RM) -r main *.o *.c~

run: $(BIN)
        ./main
```

## Style

I *try* to follow the `Linux` style, I use the `indent` package to *fix* indentation.
Make sure your editor don't convert tabs to spaces, one tab equals to 8 spaces.

```shell
indent -linux -l100 main.c
```

[01]: https://adventofcode.com/2015/day/1
[02]: https://adventofcode.com/2015/day/2
[03]: https://adventofcode.com/2015/day/3
[04]: https://adventofcode.com/2015/day/4
[05]: https://adventofcode.com/2015/day/5
[06]: https://adventofcode.com/2015/day/6
[07]: https://adventofcode.com/2015/day/7
[08]: https://adventofcode.com/2015/day/8
[09]: https://adventofcode.com/2015/day/9
[10]: https://adventofcode.com/2015/day/10
[11]: https://adventofcode.com/2015/day/11
[12]: https://adventofcode.com/2015/day/12
[13]: https://adventofcode.com/2015/day/13
[14]: https://adventofcode.com/2015/day/14
[15]: https://adventofcode.com/2015/day/15
[16]: https://adventofcode.com/2015/day/16
[17]: https://adventofcode.com/2015/day/17
[18]: https://adventofcode.com/2015/day/18
[19]: https://adventofcode.com/2015/day/19
[20]: https://adventofcode.com/2015/day/20
[21]: https://adventofcode.com/2015/day/21
[22]: https://adventofcode.com/2015/day/22
[23]: https://adventofcode.com/2015/day/23
[24]: https://adventofcode.com/2015/day/24
[25]: https://adventofcode.com/2015/day/25
