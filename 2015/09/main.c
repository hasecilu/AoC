/*
 * Advent of Code 2015
 * --- Day 9: All in a Single Night ---
 * https://adventofcode.com/2015/day/9
 * --------------------
 * Author: @hasecilu
 * 
 * Preprocess input file to get all unique keys (city names) and perfect minimal hash function
 * 	awk '{print $1 "\n" $3}' input | sort | uniq > keys && triehash keys > triehash.h
 */

#include <stdio.h>
#include "triehash.h"

#define	BUFF_SIZE	35
#define CITIES		8	// wc -l keys
#define max(a,b)	((a) > (b)) ? a : b
#define min(a,b)	((a) < (b)) ? a : b

unsigned int cities[CITIES][CITIES];	// Matrix working thanks to the hash function used
unsigned int min_distance = -1, max_distance = 0;

void add_distance(char *city_A, char *city_B, unsigned int d);
void print_adjacency_matrix();
int slen(char *s);
void brute_force_distances();
void permute(int index, int *arr, int n);
void swap(int *i, int *j);

int main()
{
	FILE *file;
	char city_A[15], city_B[15], dummy1[3], dummy2[3];
	unsigned int distance;

	file = fopen("input", "r");
	while (fscanf(file, " %s %s %s %s %d", city_A, dummy1, city_B, dummy2, &distance) == 5)
		add_distance(city_A, city_B, distance);
	fclose(file);

	print_adjacency_matrix();

	brute_force_distances();

	return 0;
}

/* Add distaces to symetric matrix */
void add_distance(char *city_A, char *city_B, unsigned int d)
{
	unsigned int A = PerfectHash(city_A, slen(city_A));
	unsigned int B = PerfectHash(city_B, slen(city_B));
	cities[A][B] = d;
	cities[B][A] = d;
}

/* Return string length */
int slen(char *s)
{
	int i;
	for (i = 0; s[i] != '\0'; ++i) ;
	return i;
}

/* Print adjacency matrix */
void print_adjacency_matrix()
{
	for (unsigned int i = 0; i < CITIES; i++) {
		for (unsigned int j = 0; j < CITIES; j++)
			printf("%d\t", cities[i][j]);
		printf("\n");
	}
}

/* Brute force, check all the permutations */
void brute_force_distances()
{
	int arr[CITIES];
	for (int i = 0; i < CITIES; ++i)
		arr[i] = i;
	permute(0, arr, CITIES);
	printf("\nThe shortest route is: %d\n", min_distance);
	printf("The longest route is: %d\n", max_distance);
}

/* Last 2 functions based on ones made by Damien@stackoverflow  */
/* https://stackoverflow.com/questions/70758782/printing-all-permutations-of-n-numbers */
void permute(int index, int *arr, int n)
{
	if (index == n - 1) {
		unsigned int distance = 0;
		for (int k = 0; k < n - 1; ++k) {
			distance += cities[arr[k]][arr[k + 1]];
			/* printf("%d %d = %d  / ", arr[k], arr[k+1], cities[arr[k]][arr[k+1]]); */
		}
		/* printf("\tdistance: %d\n", distance); */
		min_distance = min(min_distance, distance);
		max_distance = max(max_distance, distance);
		return;
	}
	for (int i = index; i < n; i++) {
		swap(arr + index, arr + i);
		permute(index + 1, arr, n);
		swap(arr + i, arr + index);
	}
	return;
}

void swap(int *i, int *j)
{
	int temp = *i;
	*i = *j;
	*j = temp;
}
