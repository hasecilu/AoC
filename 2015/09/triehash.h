#ifndef TRIE_HASH_PerfectHash
#define TRIE_HASH_PerfectHash
#include <stddef.h>
#include <stdint.h>
enum PerfectKey {
    AlphaCentauri = 0,
    Arbre = 1,
    Faerun = 2,
    Norrath = 3,
    Snowdin = 4,
    Straylight = 5,
    Tambi = 6,
    Tristram = 7,
    Unknown = -1,
};
static enum PerfectKey PerfectHash(const char *string, size_t length);
#ifdef __GNUC__
typedef uint16_t __attribute__((aligned (1))) triehash_uu16;
typedef char static_assert16[__alignof__(triehash_uu16) == 1 ? 1 : -1];
typedef uint32_t __attribute__((aligned (1))) triehash_uu32;
typedef char static_assert32[__alignof__(triehash_uu32) == 1 ? 1 : -1];
typedef uint64_t __attribute__((aligned (1))) triehash_uu64;
typedef char static_assert64[__alignof__(triehash_uu64) == 1 ? 1 : -1];
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define onechar(c, s, l) (((uint64_t)(c)) << (s))
#else
#define onechar(c, s, l) (((uint64_t)(c)) << (l-8-s))
#endif
#if (!defined(__ARM_ARCH) || defined(__ARM_FEATURE_UNALIGNED)) && !defined(TRIE_HASH_NO_MULTI_BYTE)
#define TRIE_HASH_MULTI_BYTE
#endif
#endif /*GNUC */
#ifdef TRIE_HASH_MULTI_BYTE
static enum PerfectKey PerfectHash5(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('A', 0, 32)| onechar('r', 8, 32)| onechar('b', 16, 32)| onechar('r', 24, 32):
        switch(string[4]) {
        case 0| onechar('e', 0, 8):
            return Arbre;
        }
        break;
    case 0| onechar('T', 0, 32)| onechar('a', 8, 32)| onechar('m', 16, 32)| onechar('b', 24, 32):
        switch(string[4]) {
        case 0| onechar('i', 0, 8):
            return Tambi;
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash6(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('F', 0, 32)| onechar('a', 8, 32)| onechar('e', 16, 32)| onechar('r', 24, 32):
        switch(string[4]) {
        case 0| onechar('u', 0, 8):
            switch(string[5]) {
            case 0| onechar('n', 0, 8):
                return Faerun;
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash7(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('N', 0, 32)| onechar('o', 8, 32)| onechar('r', 16, 32)| onechar('r', 24, 32):
        switch(string[4]) {
        case 0| onechar('a', 0, 8):
            switch(string[5]) {
            case 0| onechar('t', 0, 8):
                switch(string[6]) {
                case 0| onechar('h', 0, 8):
                    return Norrath;
                }
            }
        }
        break;
    case 0| onechar('S', 0, 32)| onechar('n', 8, 32)| onechar('o', 16, 32)| onechar('w', 24, 32):
        switch(string[4]) {
        case 0| onechar('d', 0, 8):
            switch(string[5]) {
            case 0| onechar('i', 0, 8):
                switch(string[6]) {
                case 0| onechar('n', 0, 8):
                    return Snowdin;
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash8(const char *string)
{
    switch(*((triehash_uu64*) &string[0])) {
    case 0| onechar('T', 0, 64)| onechar('r', 8, 64)| onechar('i', 16, 64)| onechar('s', 24, 64)| onechar('t', 32, 64)| onechar('r', 40, 64)| onechar('a', 48, 64)| onechar('m', 56, 64):
        return Tristram;
    }
    return Unknown;
}
static enum PerfectKey PerfectHash10(const char *string)
{
    switch(*((triehash_uu64*) &string[0])) {
    case 0| onechar('S', 0, 64)| onechar('t', 8, 64)| onechar('r', 16, 64)| onechar('a', 24, 64)| onechar('y', 32, 64)| onechar('l', 40, 64)| onechar('i', 48, 64)| onechar('g', 56, 64):
        switch(string[8]) {
        case 0| onechar('h', 0, 8):
            switch(string[9]) {
            case 0| onechar('t', 0, 8):
                return Straylight;
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash13(const char *string)
{
    switch(*((triehash_uu64*) &string[0])) {
    case 0| onechar('A', 0, 64)| onechar('l', 8, 64)| onechar('p', 16, 64)| onechar('h', 24, 64)| onechar('a', 32, 64)| onechar('C', 40, 64)| onechar('e', 48, 64)| onechar('n', 56, 64):
        switch(*((triehash_uu32*) &string[8])) {
        case 0| onechar('t', 0, 32)| onechar('a', 8, 32)| onechar('u', 16, 32)| onechar('r', 24, 32):
            switch(string[12]) {
            case 0| onechar('i', 0, 8):
                return AlphaCentauri;
            }
        }
    }
    return Unknown;
}
#else
static enum PerfectKey PerfectHash5(const char *string)
{
    switch(string[0]) {
    case 'A':
        switch(string[1]) {
        case 'r':
            switch(string[2]) {
            case 'b':
                switch(string[3]) {
                case 'r':
                    switch(string[4]) {
                    case 'e':
                        return Arbre;
                    }
                }
            }
        }
        break;
    case 'T':
        switch(string[1]) {
        case 'a':
            switch(string[2]) {
            case 'm':
                switch(string[3]) {
                case 'b':
                    switch(string[4]) {
                    case 'i':
                        return Tambi;
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash6(const char *string)
{
    switch(string[0]) {
    case 'F':
        switch(string[1]) {
        case 'a':
            switch(string[2]) {
            case 'e':
                switch(string[3]) {
                case 'r':
                    switch(string[4]) {
                    case 'u':
                        switch(string[5]) {
                        case 'n':
                            return Faerun;
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash7(const char *string)
{
    switch(string[0]) {
    case 'N':
        switch(string[1]) {
        case 'o':
            switch(string[2]) {
            case 'r':
                switch(string[3]) {
                case 'r':
                    switch(string[4]) {
                    case 'a':
                        switch(string[5]) {
                        case 't':
                            switch(string[6]) {
                            case 'h':
                                return Norrath;
                            }
                        }
                    }
                }
            }
        }
        break;
    case 'S':
        switch(string[1]) {
        case 'n':
            switch(string[2]) {
            case 'o':
                switch(string[3]) {
                case 'w':
                    switch(string[4]) {
                    case 'd':
                        switch(string[5]) {
                        case 'i':
                            switch(string[6]) {
                            case 'n':
                                return Snowdin;
                            }
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash8(const char *string)
{
    switch(string[0]) {
    case 'T':
        switch(string[1]) {
        case 'r':
            switch(string[2]) {
            case 'i':
                switch(string[3]) {
                case 's':
                    switch(string[4]) {
                    case 't':
                        switch(string[5]) {
                        case 'r':
                            switch(string[6]) {
                            case 'a':
                                switch(string[7]) {
                                case 'm':
                                    return Tristram;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash10(const char *string)
{
    switch(string[0]) {
    case 'S':
        switch(string[1]) {
        case 't':
            switch(string[2]) {
            case 'r':
                switch(string[3]) {
                case 'a':
                    switch(string[4]) {
                    case 'y':
                        switch(string[5]) {
                        case 'l':
                            switch(string[6]) {
                            case 'i':
                                switch(string[7]) {
                                case 'g':
                                    switch(string[8]) {
                                    case 'h':
                                        switch(string[9]) {
                                        case 't':
                                            return Straylight;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash13(const char *string)
{
    switch(string[0]) {
    case 'A':
        switch(string[1]) {
        case 'l':
            switch(string[2]) {
            case 'p':
                switch(string[3]) {
                case 'h':
                    switch(string[4]) {
                    case 'a':
                        switch(string[5]) {
                        case 'C':
                            switch(string[6]) {
                            case 'e':
                                switch(string[7]) {
                                case 'n':
                                    switch(string[8]) {
                                    case 't':
                                        switch(string[9]) {
                                        case 'a':
                                            switch(string[10]) {
                                            case 'u':
                                                switch(string[11]) {
                                                case 'r':
                                                    switch(string[12]) {
                                                    case 'i':
                                                        return AlphaCentauri;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
#endif /* TRIE_HASH_MULTI_BYTE */
static enum PerfectKey PerfectHash(const char *string, size_t length)
{
    switch (length) {
    case 5:
        return PerfectHash5(string);
    case 6:
        return PerfectHash6(string);
    case 7:
        return PerfectHash7(string);
    case 8:
        return PerfectHash8(string);
    case 10:
        return PerfectHash10(string);
    case 13:
        return PerfectHash13(string);
    default:
        return Unknown;
    }
}
#endif                       /* TRIE_HASH_PerfectHash */
