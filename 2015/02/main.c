/*
 * Advent of Code 2015
 * --- Day 2: I Was Told There Would Be No Math ---
 * https://adventofcode.com/2015/day/2
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFF_SIZE   15
#define SIDE_SIZE   3

char buff[BUFF_SIZE], n0_str[SIDE_SIZE], n1_str[SIDE_SIZE], n2_str[SIDE_SIZE];

void find_character(char c, char *s, char *spc);

int main()
{
	FILE *p;
	char num[3], aux;
	unsigned int area = 0, length = 0;

	p = fopen("data", "r");

	while (fgets(buff, BUFF_SIZE, p) != NULL) {
		char exes[4] = { };

		find_character('x', buff, exes);

		strncpy(n0_str, buff, exes[0]);
		strncpy(n1_str, buff + exes[0] + 1, (exes[1] - 1) - (exes[0]));
		strncpy(n2_str, buff + exes[1] + 1, (strlen(buff) - 1) - (exes[1]));

		num[0] = atoi(n0_str);
		num[1] = atoi(n1_str);
		num[2] = atoi(n2_str);

		if (num[0] > num[1]) {
			aux = num[0];
			num[0] = num[1];
			num[1] = aux;
		}
		if (num[1] > num[2]) {
			aux = num[1];
			num[1] = num[2];
			num[2] = aux;
		}

		/* printf("Result: %dx%dx%d\n", num[0], num[1], num[2]); */

		area += 2 * (num[1] * num[2] + num[0] * num[2]) + 3 * num[0] * num[1];
		length += 2 * (num[0] + num[1]) + num[0] * num[1] * num[2];

		memset(buff, 0, BUFF_SIZE);
		memset(n0_str, 0, SIDE_SIZE);
		memset(n1_str, 0, SIDE_SIZE);
		memset(n2_str, 0, SIDE_SIZE);
	}
	fclose(p);

	printf("Total area: %d [ft^2]\n\n", area);
	printf("Total length: %d [ft]\n", length);

	return 0;
}

/* Find character c on string s and return positions as an array. */
void find_character(char c, char *s, char *spc)
{
	unsigned char idx = 0;

	for (int i = 0; i < strlen(s) - 1; i++) {
		if (s[i] == c) {
			spc[idx] = i;
			idx++;
		}
	}
}
