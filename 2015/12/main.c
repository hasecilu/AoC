/*
 * Advent of Code 2015
 * --- Day 12: JSAbacusFramework.io ---
 * https://adventofcode.com/2015/day/12
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_DIGITS 3

char stack[MAX_DIGITS + 1];
int top = -1;

void push(char);
char pop();
int is_full();
int is_empty();

int main()
{
	FILE *file;
	char ch = '\0', negative = 0;
	int sum = 0;

	file = fopen("input", "r");
	do {
		ch = fgetc(file);

		if (ch == '-')
			negative = 1;

		if (ch >= '0' && ch <= '9')
			push(ch);

		if (!(is_empty()) && !(ch >= '0' && ch <= '9')) {
			if (negative)
				sum -= atoi(stack);
			else
				sum += atoi(stack);
			do
				pop();
			while (top != -1);

			memset(stack, '\0', MAX_DIGITS);
			negative = 0;
		}
	} while (ch != EOF);
	fclose(file);

	printf("\nSum: %d\n", sum);

	return 0;
}

void push(char c)
{
	if (is_full()) {
		printf("Stack overflow\n");
		exit(1);
	}
	top++;
	stack[top] = c;
}

char pop()
{
	char c;
	if (is_empty()) {
		printf("Stack underflow\n");
		exit(1);
	}
	c = stack[top];
	top--;
	return c;
}

int is_full()
{
	if (top == MAX_DIGITS - 1)
		return 1;
	else
		return 0;
}

int is_empty()
{
	if (top == -1)
		return 1;
	else
		return 0;
}
