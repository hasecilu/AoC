/*
 * Advent of Code 2015
 * --- Day 20: Infinite Elves and Infinite Houses ---
 * https://adventofcode.com/2015/day/20
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

#define PRESENTS 36e6
// #define PART2

unsigned int presents(unsigned int house);

int main()
{
	// Part 1 Duration time: 1291[s] = 21.5[min]
	// Part 1 Result: 831600
	// Part 2 Duration time: 24.5[min]
	// Part 2 Result: 884520
	unsigned int house = 0;
	do {
	} while (presents(++house) < PRESENTS);
	printf("House %u got %u presents.\n", house, presents(house));
	return 0;
}

/* Assign presents to house. */
unsigned int presents(unsigned int house)
{
	unsigned int presents = 0;
	for (unsigned int elf = 1; elf <= house; elf++)
#ifndef PART2
		if (house % elf == 0)
			presents += 10 * elf;
#else
		if (house % elf == 0 && house <= 50 * elf)
			presents += 11 * elf;
#endif /* ifndef PART2 */
	return presents;
}
