/*
 * Advent of Code 2015
 * --- Day 3: Perfectly Spherical Houses in a Vacuum ---
 * https://adventofcode.com/2015/day/3
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

#define ROBO_SANTA
#define MAX_L	8200
#define NORTH   '^'
#define SOUTH   'v'
#define EAST    '>'
#define WEST    '<'

int main()
{
	FILE *file;
	char ch[2];
	signed int x[MAX_L], y[MAX_L], x2[MAX_L], y2[MAX_L],
	   xS = 0, yS = 0, xRS = 0, yRS = 0,
	   h = 1, index = 0, lucky = 0;
	x[0] = 0;
	y[0] = 0;

	file = fopen("data", "r");

	do {
#ifndef ROBO_SANTA
		ch[0] = fgetc(file);
		//printf("(%d,%d)\n", xS, yS);
		switch (ch[0]) {
		case NORTH:
			yS++;
			break;
		case SOUTH:
			yS--;
			break;
		case EAST:
			xS++;
			break;
		case WEST:
			xS--;
			break;
		}
		index++;
		x[index] = xS;
		y[index] = yS;

		for (int i = 0; i < index; i++) {
			if (xS == x[i] && yS == y[i]) {
				lucky++;
				break;
			}
		}
		h += 1 - lucky;
		lucky = 0;
#else
		ch[0] = fgetc(file);	//Santa
		ch[1] = fgetc(file);	//Robo-Santa
		//printf("(%d,%d)\t(%d,%d)\t%d\n", xS, yS, xRS, yRS, h);
		switch (ch[0]) {
		case NORTH:
			yS++;
			break;
		case SOUTH:
			yS--;
			break;
		case EAST:
			xS++;
			break;
		case WEST:
			xS--;
			break;
		}
		switch (ch[1]) {
		case NORTH:
			yRS++;
			break;
		case SOUTH:
			yRS--;
			break;
		case EAST:
			xRS++;
			break;
		case WEST:
			xRS--;
			break;
		}
		index++;
		x[index] = xS;
		y[index] = yS;
		x2[index] = xRS;
		y2[index] = yRS;

		for (int i = 0; i < index; i++) {
			if ((xS == x[i] && yS == y[i])
			    || (xS == x2[i] && yS == y2[i])) {
				lucky++;
				break;
			}
		}
		for (int i = 0; i < index; i++) {
			if ((xRS == x[i] && yRS == y[i])
			    || (xRS == x2[i] && yRS == y2[i])) {
				lucky++;
				break;
			}
		}
		h += 2 - lucky;
		lucky = 0;
#endif
	} while (ch[0] != EOF);
	fclose(file);

	printf("\n\nHouses: %d\n", h);

	return 0;
}
