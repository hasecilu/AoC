/*
 * Advent of Code 2015
 * --- Day 15: Science for Hungry People ---
 * https://adventofcode.com/2015/day/15
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>

/* #define DEBUG */
#define BUFF_SIZE	75
#define TEASPOONS 	100
#define INGREDIENTS 	4
#define max(a,b)	((a) > (b)) ? a : b

struct {
	int capacity, durability, flavor, texture, calories;
} ing[INGREDIENTS];

void permutations(int sum);
void calc_score(int spoon[INGREDIENTS]);
int max_score, max_500cal;

int main()
{
	FILE *file;
	char buffer[BUFF_SIZE];
	unsigned int i = 0;

	file = fopen("input", "r");
	while (fgets(buffer, sizeof(buffer), file) != NULL) {
		char x1[3], x2[3], x3[3], x4[3], x5[3];
		if (sscanf
		    (buffer, "%*s %*s %[^,], %*s %[^,], %*s %[^,], %*s %[^,], %*s %[^,]", x1, x2,
		     x3, x4, x5) == 5) {
			ing[i].capacity = atoi(x1);
			ing[i].durability = atoi(x2);
			ing[i].flavor = atoi(x3);
			ing[i].texture = atoi(x4);
			ing[i].calories = atoi(x5);
			i++;
		}
	}
	fclose(file);

	permutations(TEASPOONS);

	printf("The best score is: %d\n", max_score);
	printf("The best score is: %d\n", max_500cal);

	return 0;
}

/* Generate all permutations that sum exactly TEASPOONS */
void permutations(int sum)
{
	for (int a = 0; a <= sum; a++) {
		for (unsigned int b = 0; b <= (sum - a); b++) {
			for (unsigned int c = 0; c <= (sum - a - b); c++) {
				unsigned int d = sum - a - b - c;
				int spoon[INGREDIENTS];
				spoon[0] = a;
				spoon[1] = b;
				spoon[2] = c;
				spoon[3] = d;
#ifdef DEBUG
				for (unsigned int i = 0; i < INGREDIENTS; i++)
					printf("%d ", spoon[i]);
				printf("\t");
#endif				/* ifdef DEBUG */
				calc_score(spoon);
			}
		}
	}
}

/* Calc the score for a particular permutation */
void calc_score(int spoon[INGREDIENTS])
{
	int cap = 0, dur = 0, fla = 0, tex = 0, cal = 0;
	for (unsigned int i = 0; i < INGREDIENTS; i++) {
		cap += spoon[i] * ing[i].capacity;
		dur += spoon[i] * ing[i].durability;
		fla += spoon[i] * ing[i].flavor;
		tex += spoon[i] * ing[i].texture;
		cal += spoon[i] * ing[i].calories;
	}
	if (cap <= 0 || dur <= 0 || fla <= 0 || tex <= 0) {
		return;
	} else {
		max_score = max(max_score, (cap * dur * fla * tex));
		if (cal == 500)
			max_500cal = max(max_500cal, (cap * dur * fla * tex));
	}
#ifdef DEBUG
	printf("%d %d %d %d\t", cap, dur, fla, tex);
	printf("%d\t", (cap * dur * fla * tex));
	printf("%d\n", max_score);
#endif				/* ifdef DEBUG */
}
