/*
 * Advent of Code 2015
 * --- Day 13: Knights of the Dinner Table ---
 * https://adventofcode.com/2015/day/13
 * --------------------
 * Author: @hasecilu
 * 
 * Preprocess input file to get all unique keys (names) and the perfect minimal hash function
 * 	awk '{print $1}' input | uniq > keys && triehash keys > triehash.h
 */

#include <stdio.h>
#include "triehash.h"

#define	BUFF_SIZE	66
#define ATTENDEES	9
#define max(a,b)	((a) > (b)) ? a : b

int people[ATTENDEES][ATTENDEES];	// Matrix working thanks to the hash function used
int max_happiness = 0;

void add_happiness(char *p1, char *p2, char *s, int h);
void print_adjacency_matrix();
int slen(char *s);
void brute_force_happinesss();
void circular_permutation(int index, int *c_perm, int n);
void swap(int *i, int *j);

int main()
{
	FILE *file;
	char buffer[BUFF_SIZE], p1[10], p2[10], s[5];
	int happiness;

	file = fopen("input", "r");
	while (fgets(buffer, sizeof(buffer), file) != NULL) {
		buffer[slen(buffer) - 2] = '\0';
		// Parse the line and extract the desired elements
		if (sscanf(buffer, "%s %*s %s %d %*s %*s %*s %*s %*s %*s %s", p1, s, &happiness, p2)
		    == 4)
			add_happiness(p1, p2, s, happiness);
	}
	fclose(file);

	print_adjacency_matrix();

	brute_force_happinesss();

	return 0;
}

/* Add distaces to symetric matrix */
void add_happiness(char *p1, char *p2, char *s, int h)
{
	if (s[0] == 'l')	// lose
		h *= -1;
	unsigned int p = PerfectHash(p1, slen(p1));
	unsigned int q = PerfectHash(p2, slen(p2));
	people[p][q] = h;
}

/* Return string length */
int slen(char *s)
{
	int i;
	for (i = 0; s[i] != '\0'; ++i) ;
	return i;
}

/* Print adjacency matrix */
void print_adjacency_matrix()
{
	for (unsigned int i = 0; i < ATTENDEES; i++) {
		for (unsigned int j = 0; j < ATTENDEES; j++)
			printf("%d\t", people[i][j]);
		printf("\n");
	}
}

/* Brute force, check all the permutations */
void brute_force_happinesss()
{
	int c_perm[ATTENDEES];	// Create complete array
	for (int i = 0; i < ATTENDEES; ++i)
		c_perm[i] = i;
	circular_permutation(0, c_perm, ATTENDEES - 1);	// But let one fixed
	printf("Happiest arrange is: %d\n", max_happiness);
}

/* One person (last position) is fixed and all other permutate */
void circular_permutation(int index, int *c_perm, int n)
{
	if (index == n - 1) {
		int happiness = 0;
		for (int k = 0; k < n; ++k) {
			int p_left = k - 1;
			if (p_left == -1)
				p_left = ATTENDEES - 1;

			happiness += people[c_perm[k]][c_perm[k + 1]];
			happiness += people[c_perm[k]][c_perm[p_left]];
			/* printf("%d %d  | ", people[c_perm[k]][k + 1], people[c_perm[k]][p_left]); */
		}
		happiness += people[c_perm[ATTENDEES - 1]][c_perm[0]];
		happiness += people[c_perm[ATTENDEES - 1]][c_perm[ATTENDEES - 2]];
		/* printf("%d %d  ", people[c_perm[ATTENDEES - 1]][0], people[c_perm[ATTENDEES - 1]][ATTENDEES - 2]); */
		/* printf("\thappiness: %d\n", happiness); */
		max_happiness = max(max_happiness, happiness);
	}
	for (int i = index; i < n; i++) {
		swap(&c_perm[index], &c_perm[i]);
		circular_permutation(index + 1, c_perm, n);
		swap(&c_perm[index], &c_perm[i]);
	}
}

void swap(int *i, int *j)
{
	int temp = *i;
	*i = *j;
	*j = temp;
}
