#ifndef TRIE_HASH_PerfectHash
#define TRIE_HASH_PerfectHash
#include <stddef.h>
#include <stdint.h>
enum PerfectKey {
    Alice = 0,
    Bob = 1,
    Carol = 2,
    David = 3,
    Eric = 4,
    Frank = 5,
    George = 6,
    Mallory = 7,
    Unknown = -1,
};
static enum PerfectKey PerfectHash(const char *string, size_t length);
#ifdef __GNUC__
typedef uint16_t __attribute__((aligned (1))) triehash_uu16;
typedef char static_assert16[__alignof__(triehash_uu16) == 1 ? 1 : -1];
typedef uint32_t __attribute__((aligned (1))) triehash_uu32;
typedef char static_assert32[__alignof__(triehash_uu32) == 1 ? 1 : -1];
typedef uint64_t __attribute__((aligned (1))) triehash_uu64;
typedef char static_assert64[__alignof__(triehash_uu64) == 1 ? 1 : -1];
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define onechar(c, s, l) (((uint64_t)(c)) << (s))
#else
#define onechar(c, s, l) (((uint64_t)(c)) << (l-8-s))
#endif
#if (!defined(__ARM_ARCH) || defined(__ARM_FEATURE_UNALIGNED)) && !defined(TRIE_HASH_NO_MULTI_BYTE)
#define TRIE_HASH_MULTI_BYTE
#endif
#endif /*GNUC */
#ifdef TRIE_HASH_MULTI_BYTE
static enum PerfectKey PerfectHash3(const char *string)
{
    switch(string[0]) {
    case 0| onechar('B', 0, 8):
        switch(string[1]) {
        case 0| onechar('o', 0, 8):
            switch(string[2]) {
            case 0| onechar('b', 0, 8):
                return Bob;
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash4(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('E', 0, 32)| onechar('r', 8, 32)| onechar('i', 16, 32)| onechar('c', 24, 32):
        return Eric;
    }
    return Unknown;
}
static enum PerfectKey PerfectHash5(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('A', 0, 32)| onechar('l', 8, 32)| onechar('i', 16, 32)| onechar('c', 24, 32):
        switch(string[4]) {
        case 0| onechar('e', 0, 8):
            return Alice;
        }
        break;
    case 0| onechar('C', 0, 32)| onechar('a', 8, 32)| onechar('r', 16, 32)| onechar('o', 24, 32):
        switch(string[4]) {
        case 0| onechar('l', 0, 8):
            return Carol;
        }
        break;
    case 0| onechar('D', 0, 32)| onechar('a', 8, 32)| onechar('v', 16, 32)| onechar('i', 24, 32):
        switch(string[4]) {
        case 0| onechar('d', 0, 8):
            return David;
        }
        break;
    case 0| onechar('F', 0, 32)| onechar('r', 8, 32)| onechar('a', 16, 32)| onechar('n', 24, 32):
        switch(string[4]) {
        case 0| onechar('k', 0, 8):
            return Frank;
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash6(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('G', 0, 32)| onechar('e', 8, 32)| onechar('o', 16, 32)| onechar('r', 24, 32):
        switch(string[4]) {
        case 0| onechar('g', 0, 8):
            switch(string[5]) {
            case 0| onechar('e', 0, 8):
                return George;
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash7(const char *string)
{
    switch(*((triehash_uu32*) &string[0])) {
    case 0| onechar('M', 0, 32)| onechar('a', 8, 32)| onechar('l', 16, 32)| onechar('l', 24, 32):
        switch(string[4]) {
        case 0| onechar('o', 0, 8):
            switch(string[5]) {
            case 0| onechar('r', 0, 8):
                switch(string[6]) {
                case 0| onechar('y', 0, 8):
                    return Mallory;
                }
            }
        }
    }
    return Unknown;
}
#else
static enum PerfectKey PerfectHash3(const char *string)
{
    switch(string[0]) {
    case 'B':
        switch(string[1]) {
        case 'o':
            switch(string[2]) {
            case 'b':
                return Bob;
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash4(const char *string)
{
    switch(string[0]) {
    case 'E':
        switch(string[1]) {
        case 'r':
            switch(string[2]) {
            case 'i':
                switch(string[3]) {
                case 'c':
                    return Eric;
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash5(const char *string)
{
    switch(string[0]) {
    case 'A':
        switch(string[1]) {
        case 'l':
            switch(string[2]) {
            case 'i':
                switch(string[3]) {
                case 'c':
                    switch(string[4]) {
                    case 'e':
                        return Alice;
                    }
                }
            }
        }
        break;
    case 'C':
        switch(string[1]) {
        case 'a':
            switch(string[2]) {
            case 'r':
                switch(string[3]) {
                case 'o':
                    switch(string[4]) {
                    case 'l':
                        return Carol;
                    }
                }
            }
        }
        break;
    case 'D':
        switch(string[1]) {
        case 'a':
            switch(string[2]) {
            case 'v':
                switch(string[3]) {
                case 'i':
                    switch(string[4]) {
                    case 'd':
                        return David;
                    }
                }
            }
        }
        break;
    case 'F':
        switch(string[1]) {
        case 'r':
            switch(string[2]) {
            case 'a':
                switch(string[3]) {
                case 'n':
                    switch(string[4]) {
                    case 'k':
                        return Frank;
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash6(const char *string)
{
    switch(string[0]) {
    case 'G':
        switch(string[1]) {
        case 'e':
            switch(string[2]) {
            case 'o':
                switch(string[3]) {
                case 'r':
                    switch(string[4]) {
                    case 'g':
                        switch(string[5]) {
                        case 'e':
                            return George;
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
static enum PerfectKey PerfectHash7(const char *string)
{
    switch(string[0]) {
    case 'M':
        switch(string[1]) {
        case 'a':
            switch(string[2]) {
            case 'l':
                switch(string[3]) {
                case 'l':
                    switch(string[4]) {
                    case 'o':
                        switch(string[5]) {
                        case 'r':
                            switch(string[6]) {
                            case 'y':
                                return Mallory;
                            }
                        }
                    }
                }
            }
        }
    }
    return Unknown;
}
#endif /* TRIE_HASH_MULTI_BYTE */
static enum PerfectKey PerfectHash(const char *string, size_t length)
{
    switch (length) {
    case 3:
        return PerfectHash3(string);
    case 4:
        return PerfectHash4(string);
    case 5:
        return PerfectHash5(string);
    case 6:
        return PerfectHash6(string);
    case 7:
        return PerfectHash7(string);
    default:
        return Unknown;
    }
}
#endif                       /* TRIE_HASH_PerfectHash */
