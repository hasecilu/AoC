/*
 * Advent of Code 2015
 * --- Day 8: Matchsticks ---
 * https://adventofcode.com/2015/day/8
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <string.h>

#define	BUFF_SIZE   50

typedef unsigned char u8;
typedef unsigned short u16;

int main()
{
	FILE *p;
	char buffer[BUFF_SIZE];
	u16 diff[2];

	p = fopen("data", "r");

	while (fgets(buffer, BUFF_SIZE, p) != NULL) {
		char len = strlen(buffer) - 1;	// Exclude \n
		char mem = 0, exp = 2;

		/* Part 1 */
		for (u8 i = 1; i < len - 1; ++i) {	// Exclude first and last "
			if (buffer[i] == '\\') {	// Skip scaped characters
				switch (buffer[i + 1]) {
				case '\\':
					i++;
					break;
				case '"':
					i++;
					break;
				case 'x':
					i += 3;
					break;
				default:
					break;
				}
			}
			mem++;
		}

		/* Part 2 */
		for (u8 i = 0; i < len; ++i) {
			switch (buffer[i]) {
			case '\\':
				exp += 2;
				break;
			case '"':
				exp += 2;
				break;
			default:
				exp++;
				break;
			}
		}

		diff[0] += len - mem;
		diff[1] += exp - len;
		printf("code: %d\tmemory: %d\texpand: %d\tdiff1: %d\tdiff2: %d\n",
		     len, mem, exp, diff[0], diff[1]);
	}

	fclose(p);

	return 0;
}
