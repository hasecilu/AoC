/*
 * Advent of Code 2015
 * --- Day 7: Some Assembly Required ---
 * https://adventofcode.com/2015/day/7
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <string.h>

/* #define DEBUG0 */
/* #define DEBUG1 */
#define	BUFF_SIZE   20
#define DATA_LEN    339

typedef unsigned short u16;

typedef enum { UNDEFINED = -1, BUFFER, NOT, AND, OR, LSHIFT, RSHIFT
} operations;

typedef struct {
	char in0_name[6];	// Input 0 string, also used for seed
	char in1_name[4];	// Input 1 string, also used for shift
	char out_name[4];	// Output string
	u16 in0;		// Input 0 value
	u16 in1;		// Input 1 value
	u16 out;		// Output value
	char done;		// State if operation was already done
	operations op;		// Logic operation to realize
} Instruction;

void find_character(char c, char *s, char *spc);
int str2int(char *str);
void print_instruction(Instruction ins);
void reset_wires(Instruction * ins);
void fill_instruction(Instruction * ins, char buffer[BUFF_SIZE], u16 index);
void solve_instructions(Instruction ins[DATA_LEN]);

int main()
{
	FILE *p;
	char buffer[BUFF_SIZE];

	Instruction ins[DATA_LEN];
	memset(ins, 0, sizeof(ins));
	u16 index = 0;

	p = fopen("data", "r");

	/* Read the data from the file and save in the struct array. */
	while (fgets(buffer, BUFF_SIZE, p) != NULL) {
		fill_instruction(&ins[index], buffer, index);
		index++;
	}
	fclose(p);

#ifdef DEBUG0
	printf("\n\nINITIAL CONDITIONS PART 1\n");
	printf("\ninstr\tin0_n\tin1_n\tout_n\tin0\tin1\tout\tdone\top\n");
	printf("-------------------------------------------------------------------\n");
	for (u16 i = 0; i < DATA_LEN; i++) {
		printf("%d\t", i + 1);
		print_instruction(ins[i]);
	}
	getchar();
	printf("Solving wiring\n\n");
#endif				/* ifdef DEBUG */

	solve_instructions(ins);

	/* Print all final results */
	/* printf("\n\nResults:\n\n"); */
	/* for (u16 i = 0; i < DATA_LEN; i++) */
	/*     printf("%s: %d\n", ins[i].out_name, ins[i].out); */

	u16 bridge = 0;
	for (u16 ii = 0; ii < DATA_LEN; ii++) {
		if (strcmp("a", ins[ii].out_name) == 0) {
			printf("\n\nPart 1: a -> %d\n\n\n", ins[ii].out);
			bridge = ins[ii].out;
			break;
		}
	}

	/* getchar(); */

	/* --------------- PART 2 --------------- */

	/* Reset previous output data restoring the value of 'a' */
	for (u16 ii = 0; ii < DATA_LEN; ii++)
		reset_wires(&ins[ii]);

	for (u16 ii = 0; ii < DATA_LEN; ii++) {
		if (strcmp("b", ins[ii].out_name) == 0) {
			ins[ii].out = bridge;
			ins[ii].done = 1;
			break;
		}
	}

#ifdef DEBUG0
	printf("\n\nINITIAL CONDITIONS PART 2\n");
	printf("\ninstr\tin0_n\tin1_n\tout_n\tin0\tin1\tout\tdone\top\n");
	printf("-------------------------------------------------------------------\n");
	for (u16 i = 0; i < DATA_LEN; i++) {
		printf("%d\t", i + 1);
		print_instruction(ins[i]);
	}
	getchar();
	printf("Solving wiring\n\n");
#endif				/* ifdef DEBUG */

	solve_instructions(ins);

	for (u16 ii = 0; ii < DATA_LEN; ii++) {
		if (strcmp("a", ins[ii].out_name) == 0) {
			printf("Part 2: a -> %d\n", ins[ii].out);
			break;
		}
	}

	return 0;
}

/* Find character c on string s and return positions as an array. */
void find_character(char c, char *s, char *spc)
{
	unsigned char idx = 0;

	for (int i = 0; i < strlen(s) - 1; i++) {
		if (s[i] == c) {
			spc[idx] = i;
			idx++;
		}
	}
}

/* Convert string to integer */
int str2int(char *str)
{
	short res = 0;

	for (int i = 0; str[i] != '\0'; ++i)
		res = res * 10 + str[i] - '0';

	return res;
}

/* Print all elements of one Instruction instance. Using pass by value. */
void print_instruction(Instruction ins)
{
	printf("'%s'\t'%s'\t'%s'\t%d\t%d\t%d\t%d\t%d\n",
	       ins.in0_name, ins.in1_name, ins.out_name,
	       ins.in0, ins.in1, ins.out, ins.done, ins.op);
}

/* Reset outputs to solve part 2. */
void reset_wires(Instruction *ins)
{
	ins->out = 0;
	ins->done = 0;
}

/* Write data from text file. Using pass by reference. */
void fill_instruction(Instruction *ins, char buffer[BUFF_SIZE], u16 index)
{
	char spaces[4] = { };

	find_character(' ', buffer, spaces);

	if (spaces[2] == 0) {	// BUFFER
		/* One way to dereference the pointer is to use: (*pointer).member */
		strncpy((*ins).in0_name, buffer, spaces[0]);
		strncpy((*ins).out_name, buffer + spaces[1] + 1, (strlen(buffer) - 1) - (spaces[1] + 1));
		(*ins).op = BUFFER;
		if (buffer[0] >= '0' && buffer[0] <= '9') {	// SEED
			(*ins).in0 = str2int((*ins).in0_name);
			(*ins).out = (*ins).in0;
			(*ins).done = 1;
		}
	} else {
		/* Another way to dereference the pointer is to use: pointer->member */
		if (buffer[0] == 'N') {	// NOT
			strncpy(ins->in0_name, buffer + spaces[0] + 1, spaces[1] - (spaces[0] + 1));
			strncpy(ins->out_name, buffer + spaces[2] + 1, (strlen(buffer) - 1) - (spaces[2] + 1));
			ins->op = NOT;
		} else {
			strncpy(ins->in0_name, buffer, spaces[0]);
			strncpy(ins->in1_name, buffer + spaces[1] + 1, spaces[2] - (spaces[1] + 1));
			strncpy(ins->out_name, buffer + spaces[3] + 1, (strlen(buffer) - 1) - (spaces[3] + 1));

			if (buffer[spaces[0] + 1] == 'A')	// AND
				ins->op = AND;
			else if (buffer[spaces[0] + 1] == 'O')	// OR
				ins->op = OR;
			else if (buffer[spaces[0] + 1] == 'L') {	// LSHIFT
				ins->in1 = str2int(ins->in1_name);
				ins->op = LSHIFT;
			} else if (buffer[spaces[0] + 1] == 'R') {	// RSHIFT
				ins->in1 = str2int(ins->in1_name);
				ins->op = RSHIFT;
			}
		}
	}
}

/* Process the data to determine outputs, brute force. */
void solve_instructions(Instruction ins[DATA_LEN])
{
	u16 finish = 0;

	do {
		for (u16 i = 0; i < DATA_LEN; i++) {
			if (ins[i].done == 0) {	// Operation still not done
#ifdef DEBUG0
				printf("Testing instruction %d:\t\t", i + 1);
#endif				/* ifdef DEBUG0 */
				switch (ins[i].op) {
				case BUFFER:
#ifdef DEBUG0
					printf("BUFFER %s -> %s\n", ins[i].in0_name, ins[i].out_name);
#endif				/* ifdef DEBUG0 */
					/* Last minute & very specific hard code to finish part 2  */
					if (ins[i].in0_name[0] == '0') {
						ins[i].out = 0;
						ins[i].done = 1;
					} else {
						/* If all the inputs to calculate the output are ready, do it! */
						for (u16 ii = 0; ii < DATA_LEN; ii++) {
							if (ins[ii].done) {
								if (strcmp(ins[i].in0_name, ins[ii].out_name) == 0) {
									ins[i].out = ins[ii].out;
									ins[i].done = 1;
									break;
								}
							}
						}
					}
					break;
				case NOT:
#ifdef DEBUG0
					printf("NOT %s -> %s\n", ins[i].in0_name, ins[i].out_name);
#endif				/* ifdef DEBUG0 */
					/* If all the inputs to calculate the output are ready, do it! */
					for (u16 ii = 0; ii < DATA_LEN; ii++) {
						if (ins[ii].done) {
							if (strcmp(ins[i].in0_name, ins[ii].out_name) == 0) {
								ins[i].out = ~(ins[ii].out);
								ins[i].done = 1;
								break;
							}
						}
					}
					break;
				case AND:
#ifdef DEBUG0
					printf("%s AND %s -> %s\n", ins[i].in0_name, ins[i].in1_name, ins[i].out_name);
#endif				/* ifdef DEBUG0 */
					/* In the data there is a soecial case where the signal '1' is supplied on in0 */
					/* to the AND logic gate, no other number nor supplied to OR or RSHIFT/LSHIFT */
					/* So here is a hard-coded solution to that abnormality */
					if (ins[i].in0_name[0] == '1') {
						for (u16 ii = 0; ii < DATA_LEN; ii++) {
							if (ins[ii].done) {
								if (strcmp(ins[i].in1_name, ins[ii].out_name) == 0) {
									ins[i].out = 1 & (ins[ii].out);
									ins[i].done = 1;
									/* printf("%d\t%d", ins[ii].out, ins[i].out); */
									break;
								}
							}
						}
					}
					/* If all the inputs to calculate the output are ready, do it! */
					else {
						for (u16 ii = 0; ii < DATA_LEN; ii++) {
							if (ins[ii].done) {
								if (strcmp(ins[i].in0_name, ins[ii].out_name) == 0 ||
								    strcmp(ins[i].in1_name, ins[ii].out_name) == 0) {
									for (u16 iii = ii + 1; iii < DATA_LEN; iii++) {
										if (ins[iii].done) {
											if (strcmp(ins[i].in0_name, ins[iii].out_name) == 0 ||
											    strcmp(ins[i].in1_name, ins[iii].out_name) == 0) {
												/* Logic gates are commutative */
												ins[i].out = (ins[ii].out) & (ins[iii].out);
												ins[i].done = 1;
												/* printf("%d & %d -> %d\n", ins[ii].out, ins[iii].out, ins[i].out); */
												goto byeAND;
											}
										}
									}
								}
							}
						}
					}
 byeAND:
					break;
				case OR:
#ifdef DEBUG0
					printf("%s OR %s -> %s\n", ins[i].in0_name, ins[i].in1_name, ins[i].out_name);
#endif				/* ifdef DEBUG0 */
					/* If all the inputs to calculate the output are ready, do it! */
					for (u16 ii = 0; ii < DATA_LEN; ii++) {
						if (ins[ii].done) {
							if (strcmp(ins[i].in0_name, ins[ii].out_name) == 0 ||
							    strcmp(ins[i].in1_name, ins[ii].out_name) == 0) {
								for (u16 iii = ii + 1; iii < DATA_LEN; iii++) {
									if (ins[iii].done) {
										if (strcmp(ins[i].in0_name, ins[iii].out_name) == 0 ||
										    strcmp(ins[i].in1_name, ins[iii].out_name) == 0) {
											/* Logic gates are commutative */
											ins[i].out = (ins[ii].out) | (ins[iii].out);
											ins[i].done = 1;
											/* printf("%d | %d -> %d\n", ins[ii].out, ins[iii].out, ins[i].out); */
											goto byeOR;
											break;
										}
									}
								}
							}
						}
					}
 byeOR:
					break;
				case RSHIFT:
#ifdef DEBUG0
					printf("%s RSHIFT %d -> %s\n", ins[i].in0_name, ins[i].in1, ins[i].out_name);
#endif				/* ifdef DEBUG0 */
					/* If all the inputs to calculate the output are ready, do it! */
					for (u16 ii = 0; ii < DATA_LEN; ii++) {
						if (ins[ii].done) {
							if (strcmp(ins[i].in0_name, ins[ii].out_name) == 0) {
								ins[i].out = ins[ii].out >> ins[i].in1;
								ins[i].done = 1;
								/* printf("%d\t%d", ins[ii].out, ins[i].out); */
								break;
							}
						}
					}
					break;
				case LSHIFT:
#ifdef DEBUG0
					printf("%s LSHIFT %d -> %s\n", ins[i].in0_name, ins[i].in1, ins[i].out_name);
#endif				/* ifdef DEBUG0 */
					/* If all the inputs to calculate the output are ready, do it! */
					for (u16 ii = 0; ii < DATA_LEN; ii++) {
						if (ins[ii].done) {
							if (strcmp(ins[i].in0_name, ins[ii].out_name) == 0) {
								ins[i].out = ins[ii].out << ins[i].in1;
								ins[i].done = 1;
								/* printf("%d\t%d", ins[ii].out, ins[i].out); */
								break;
							}
						}
					}
					break;
				default:
#ifdef DEBUG0
					printf("DEFAULT\n");
#endif				/* ifdef DEBUG0 */
					break;
				}
			}
		}

#ifdef DEBUG1
		tries++;
		if (tries % 50 == 0) {
			printf("\ninstr\tin0_n\tin1_n\tout_n\tin0\tin1\tout\tdone\top\n");
			printf("-------------------------------------------------------------------\n");
			for (u16 i = 0; i < DATA_LEN; i++) {
				printf("%d\t", i + 1);
				print_instruction(ins[i]);
			}
			getchar();
		}
#endif

		/* Check all operations were done and exit */
		finish = 0;
		for (u16 i = 0; i < DATA_LEN; i++)
			if (ins[i].done)
				finish++;
		/* printf("%d\n", finish); */
	} while (finish < DATA_LEN);
}
