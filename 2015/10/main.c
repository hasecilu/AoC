/*
 * Advent of Code 2015
 * --- Day 10: Elves Look, Elves Say ---
 * https://adventofcode.com/2015/day/10
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <string.h>

#define REPS	50

char input[5000000] = "3113322113";
char output[5000000] = "";

void next_sequence(char *in, char *out);

int main()
{
	for (unsigned int i = 0; i < REPS; i++) {
		next_sequence(input, output);

		printf("%d - Length is: %ld\n", i, strlen(input));
		memset(input, '\0', strlen(input));
		strcpy(input, output);
		memset(output, '\0', strlen(output));
	}

	printf("The final length is: %ld\n", strlen(input));

	return 0;
}

/* Generate the next string based in the input string */
void next_sequence(char *in, char *out)
{
	unsigned int l = strlen(in);
	int appereances = 1;

	for (unsigned int j = 1; j <= l; j++) {
		if (in[j] == in[j - 1]) {
			appereances++;
		} else {
			unsigned int ll = strlen(out);
			out[ll] = appereances + '0';
			out[ll + 1] = in[j - 1];
			appereances = 1;
		}
	}
}
