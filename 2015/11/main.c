/*
 * Advent of Code 2015
 * --- Day 11: Corporate Policy ---
 * https://adventofcode.com/2015/day/11
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

#define P_LENGTH 8

char password[P_LENGTH] = "hxbxwxba", last = 'z', flag;

int main()
{
	do {
		password[7]++;
		flag = 0;

		// Check one increasing straight of at least three letters
		for (unsigned int i = 0; i < P_LENGTH - 2; i++) {
			if (password[i + 1] == password[i] + 1
			    && password[i + 2] == password[i] + 2) {
				flag = 1;
				break;
			}
		}
		// Reject 'i' and 'l'
		for (unsigned int i = 0; i < P_LENGTH && flag; i++) {
			if (password[i] == 'i' || password[i] == 'l') {
				flag = 0;
				break;
			}
		}
		// Check at least two different, non-overlapping pairs of letters
		unsigned int pairs = 0;
		for (unsigned int i = 0; i < P_LENGTH - 1 && flag; i++) {
			if (password[i] == password[i + 1]) {
				pairs++;
				i++;
			}
		}
		if (pairs < 2)
			flag = 0;

		if (flag) {
			printf("The next password is: ");
			for (unsigned char i = 0; i < P_LENGTH; i++)
				printf("%c", password[i]);
			getchar();
		}

		for (unsigned int i = P_LENGTH - 1; i > 0; i--) {
			if (password[i] == last + 1) {
				password[i] = 'a';
				password[i - 1]++;
			}
		}
	}
	while (!
	       (password[0] == last && password[1] == last
		&& password[2] == last && password[3] == last
		&& password[4] == last && password[5] == last
		&& password[6] == last && password[7] == last));
	return 0;
}
