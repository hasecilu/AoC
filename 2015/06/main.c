/*
 * Advent of Code 2015
 * --- Day 6: Probably a Fire Hazard ---
 * https://adventofcode.com/2015/day/6
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <string.h>

#define	BUFF_SIZE   35
#define ROWS        1000
#define COLS        1000

typedef unsigned short u16;

enum operations { ON, OFF, TOGGLE, PRINT };

void find_character(char c, char *s, char *spc);
int str2int(char *str);
int subs(char *str, char p0, char p1);
void operation(char l[ROWS][COLS], enum operations op, u16 x0, u16 yo, u16 x1, u16 y1);

int main()
{
	FILE *p;
	char buffer[BUFF_SIZE];
	char lights[ROWS][COLS] = { };

	p = fopen("data", "r");

	while (fgets(buffer, BUFF_SIZE, p) != NULL) {
		u16 x0, y0, x1, y1;
		char spaces[4] = { };
		char commas[2] = { };

		find_character(' ', buffer, spaces);
		find_character(',', buffer, commas);

		if (spaces[0] == 6) {	// TOGGLE
			x0 = subs(buffer, spaces[0] + 1, commas[0]);
			y0 = subs(buffer, commas[0] + 1, spaces[1]);
			x1 = subs(buffer, spaces[2] + 1, commas[1]);
			y1 = subs(buffer, commas[1] + 1, strlen(buffer) - 1);
			/* printf("%d,%d\t%d,%d\n", x0, y0, x1, y1); */
			operation(lights, TOGGLE, x0, y0, x1, y1);
		} else {	// TURN
			x0 = subs(buffer, spaces[1] + 1, commas[0]);
			y0 = subs(buffer, commas[0] + 1, spaces[2]);
			x1 = subs(buffer, spaces[3] + 1, commas[1]);
			y1 = subs(buffer, commas[1] + 1, strlen(buffer) - 1);
			/* printf("%d,%d\t%d,%d\n", x0, y0, x1, y1); */
			if (buffer[6] == 'f') {	// OFF
				operation(lights, OFF, x0, y0, x1, y1);
			} else if (buffer[6] == 'n') {	// ON
				operation(lights, ON, x0, y0, x1, y1);
			}
		}
	}

	fclose(p);

	/* int l_ON = 0; */
	long l_bright = 0;

	for (u16 y = 0; y < ROWS; y++)
		for (u16 x = 0; x < COLS; x++)
			l_bright += lights[y][x];
	/* if ((lights[y][x]) ) */
	/*     l_ON++; */

	/* printf("\n\n%d lights ON\n", l_ON); */
	printf("\n\n%lu brightness value\n", l_bright);

	return 0;
}

/* Find character c on string s and return positions as an array */
void find_character(char c, char *s, char *spc)
{
	unsigned char idx = 0;

	for (int i = 0; i < strlen(s) - 1; i++) {
		if (s[i] == c) {
			spc[idx] = i;
			idx++;
		}
	}
}

/* Convert string to integer */
int str2int(char *str)
{
	int res = 0;

	for (int i = 0; str[i] != '\0'; ++i)
		res = res * 10 + str[i] - '0';

	return res;
}

/* Convert substring from position p0 to p1 to integer */
int subs(char *str, char p0, char p1)
{
	char number[4] = { };
	unsigned char n = 0;
	for (unsigned char i = p0; i < p1; i++) {
		number[n] = str[i];
		n++;
	}
	number[n] = '\0';
	/* printf("%s\n", number); */
	return str2int(number);
}

/* Change state of lights */
void operation(char l[ROWS][COLS], enum operations op, u16 x0, u16 y0, u16 x1, u16 y1)
{

	if (op == PRINT) {
		for (u16 y = y0; y <= y1; y++) {
			for (u16 x = x0; x <= x1; x++)
				printf("%d ", l[y][x]);
			printf("\n");
		}
	} else {
		for (u16 y = y0; y <= y1; y++) {
			for (u16 x = x0; x <= x1; x++) {
				switch (op) {
					/* Part 1 */
					/* case ON:        l[y][x] =  1;   break; */
					/* case OFF:       l[y][x] =  0;   break; */
					/* case TOGGLE:    l[y][x] ^= 1;   break; */

					/* Part 2 */
				case ON:
					l[y][x]++;
					break;
				case OFF:
					l[y][x] = (l[y][x] - 1) > 0 ? l[y][x] - 1 : 0;
					break;
				case TOGGLE:
					l[y][x] += 2;
					break;
				default:
					break;
				}
			}
		}
	}
}
