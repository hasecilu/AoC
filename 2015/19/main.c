/*
 * Advent of Code 2015
 * --- Day 19: Medicine for Rudolph ---
 * https://adventofcode.com/2015/day/19
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* #define DEBUG */
#define MAX_ELEMENTS 13
#define MAX_REPLACES 10
#define MAX_MOL_SIZE 507

struct element {
	char name[3];
	int n_repl;
	char *repl[MAX_REPLACES];	// Pointers to save the replacements
} elms[MAX_ELEMENTS];

char medicine_molecule[MAX_MOL_SIZE], *out_mols[555];
int p = -1, steps;

void read_input();
void make_replacements(char in_mol[MAX_MOL_SIZE]);
void alchemy(char el[3], char in_mol[MAX_MOL_SIZE], char out_mol[MAX_MOL_SIZE], unsigned int index,
	     size_t e, size_t r);
void transmutation();
void equivalent_exchange(unsigned int taken_steps, char in[MAX_MOL_SIZE]);

int main()
{
	// Initialize all elements of the elms array
	for (size_t e = 0; e < MAX_ELEMENTS; e++) {
		memset(&elms[e], 0, sizeof(struct element));
		for (size_t r = 0; r < MAX_REPLACES; r++) {
			elms[e].repl[r] = NULL;	// Set each repl pointer to NULL
		}
	}

	read_input();

	make_replacements(medicine_molecule);

	printf("\nThere are %d distinct molecules\n\n", p + 1);

	transmutation();

	return 0;
}

/* Read input file and parse data */
void read_input()
{
	FILE *file = fopen("input", "r");
	char *line = NULL;
	size_t line_length = 0;
	ssize_t read;
	int current_e = -1;
	char prev_elem[3] = "";

	/* Testing getline instead of fgets, quite different */
	/* while (fgets(buffer, sizeof(buffer), file) != NULL) { */
	while ((read = getline(&line, &line_length, file)) != -1) {
		if (strlen(line) > 1 && strlen(line) < 500) {	// Element + replacements
			char elem[3], repl[16];
			if (sscanf(line, "%s => %s", elem, repl) == 2) {
				if (strcmp(prev_elem, elem) != 0) {	// Different
					current_e++;
					elms[current_e].n_repl = 0;
					strcpy(elms[current_e].name, elem);
					strcpy(prev_elem, elem);
					/* printf("New e: %s\n", elms[current_e].name); */
				}
				elms[current_e].repl[elms[current_e].n_repl] =
				    (char *)malloc(strlen(repl) * sizeof(char));
				strcpy(elms[current_e].repl[elms[current_e].n_repl], repl);
				elms[current_e].n_repl++;
			}
			if (strlen(line) == 7) {
				strcpy(medicine_molecule, line);
			}
		} else if (strlen(line) > 500) {
			strcpy(medicine_molecule, line);
		}
	}
	fclose(file);

#ifdef DEBUG
	for (size_t e = 0; e < MAX_ELEMENTS; e++) {
		printf("Name: %s\n", elms[e].name);
		printf("n_repl: %d\n", elms[e].n_repl);
		for (size_t r = 0; r < elms[e].n_repl; r++)
			printf("\t%s\n", elms[e].repl[r]);
	}
	printf("Medicine molecule: <%s> %d\n", medicine_molecule, strlen(medicine_molecule));
	medicine_molecule[strcspn(medicine_molecule, "\n")] = 0;
	printf("Medicine molecule: <%s> %d\n", medicine_molecule, strlen(medicine_molecule));
#endif				/* ifdef DEBUG */
}

/* Manage all replacements available for a given molecule */
void make_replacements(char in_mol[MAX_MOL_SIZE])
{
	unsigned int index = 0;
	do {
		// Pick one element from the medicine molecule
		char el[3] = "";
		el[0] = in_mol[index];
		if (in_mol[index + 1] >= 'a') {	// 2-letter element
			el[1] = in_mol[index + 1];
			el[2] = '\0';
			index++;
		} else {
			el[1] = '\0';
			el[2] = '\0';
		}

		// Traverse all known elements
		for (size_t e = 0; e < MAX_ELEMENTS; e++)
			// Check if there is a coincidence for current element
			if (strcmp(elms[e].name, el) == 0)
				// Make all replacements available
				for (size_t r = 0; r < elms[e].n_repl; r++) {
					char out_mol[MAX_MOL_SIZE];
					alchemy(el, in_mol, out_mol, index, e, r);

					int pp = 0, different = 1;

					if (p == -1) {	// Global variable
						p++;
					} else {
						do {
							if (strcmp(out_mols[pp], out_mol) == 0)	//Same out_mol
								different = 0;
							pp++;
						} while (pp <= p && different);
						if (different)
							p++;
					}
					if (different) {
						out_mols[p] =
						    (char *)malloc(strlen(out_mol) * sizeof(char));
						strcpy(out_mols[p], out_mol);
					}
				}
		index++;
	} while (index < strlen(in_mol));
}

/* Make a replacement, split in_mol in two in the "el" position, index is auxiliary */
/* Copy the part before "el" from "in_mol" */
/* Copy the replacement "r" for the element "e"  */
/* Copy the part after "el" from "in_mol" */
void alchemy(char el[3], char in_mol[MAX_MOL_SIZE], char out_mol[MAX_MOL_SIZE],
	     unsigned int index, size_t e, size_t r)
{
	// 錬金術
	char product[MAX_MOL_SIZE] = "";
	/* out_mol = ""; */
#ifdef DEBUG
	printf("repl %d, el: %s, el_s: %d, index: %d, i: %ld, r: %ld\n", p, el, strlen(el), index,
	       e, r);
#endif				/* ifdef DEBUG */
	if (index > 0)
		strncat(product, in_mol, index - strlen(el) + 1);
#ifdef DEBUG
	printf("%d p1: %s\n", index, product);
#endif				/* ifdef DEBUG */
	strncat(product, elms[e].repl[r], strlen(elms[e].repl[r]));
#ifdef DEBUG
	printf("%d p2: %s\n", strlen(elms[e].repl[r]), product);
#endif				/* ifdef DEBUG */
	strncat(product, in_mol + index + 1, strlen(in_mol) - index);
#ifdef DEBUG
	printf("%d p3: %s\n", strlen(in_mol) - index, product);
#endif				/* ifdef DEBUG */
	strcat(product, "\0");
	strcpy(out_mol, product);
}

/* Make transmutations to generate the medicine */
void transmutation()
{
	// 錬成
	size_t e_pos = 0;
	// Find electron position
	for (size_t e = 0; e < MAX_ELEMENTS; e++)
		if (strcmp(elms[e].name, "e") == 0)
			e_pos = e;

	// Let's try all available replacements for electron to find the <<Philosopher's stone>>
	for (size_t r = 0; r < elms[e_pos].n_repl; r++) {
		printf("Start: %s\n", elms[e_pos].repl[r]);
		equivalent_exchange(1, elms[e_pos].repl[r]);	// Initial condition
	}
}

/* Apply replacements recursively to find the medicine */
void equivalent_exchange(unsigned int taken_steps, char in_mol[MAX_MOL_SIZE])
{
	// 等価交換
	// "In order to obtain or create something, something of equal value must be lost or destroyed."

#ifdef DEBUG
	printf("New ee-> steps:%d, %s %d\n", taken_steps + 1, in_mol, strlen(in_mol));
#endif				/* ifdef DEBUG */

	/* char c = getchar(); */
	if (strlen(in_mol) > strlen(medicine_molecule)) {
		printf("Big in\n");
		return;
	} else if (strlen(in_mol) == 6 && strncmp(in_mol, medicine_molecule, 6) == 0) {
		printf("Medicine molecule (%s) transmutated, %d steps.\n", in_mol, taken_steps);
		/* char c = getchar(); */
		return;
	} else {
		unsigned int index = 0;
		do {
			// Pick one element from the medicine molecule
			char el[3] = "";
			el[0] = in_mol[index];
			if (in_mol[index + 1] >= 'a') {	// 2-letter element
				el[1] = in_mol[index + 1];
				el[2] = '\0';
				index++;
			} else {
				el[1] = '\0';
				el[2] = '\0';
			}

			// Traverse all known elements
			for (size_t e = 0; e < MAX_ELEMENTS; e++)
				// Check if there is a coincidence for current element
				if (strcmp(elms[e].name, el) == 0)
					// Make all replacements available
					for (size_t r = 0; r < elms[e].n_repl; r++) {
						char out_mol[MAX_MOL_SIZE];
						alchemy(el, in_mol, out_mol, index, e, r);
						if (strlen(out_mol) > strlen(medicine_molecule)) {
							// Stop
						} else {
							// Keep looking
							equivalent_exchange(taken_steps + 1,
									    out_mol);
						}
					}
			index++;
		} while (index < strlen(in_mol));
	}
	return;
}
