/*
 * Advent of Code 2015
 * --- Day 21: RPG Simulator 20XX ---
 * https://adventofcode.com/2015/day/21
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_COMBINATIONS 20
#define MAX_ELEMENTS_PER_COMBINATION 2
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

int weapon_combinations[MAX_COMBINATIONS][MAX_ELEMENTS_PER_COMBINATION];
int armor_combinations[MAX_COMBINATIONS][MAX_ELEMENTS_PER_COMBINATION];
int ring1_combinations[MAX_COMBINATIONS][MAX_ELEMENTS_PER_COMBINATION];
int ring2_combinations[MAX_COMBINATIONS][MAX_ELEMENTS_PER_COMBINATION];

typedef struct {
	const char *name;
	unsigned int cost;
	unsigned int damage;
	unsigned int armor;
} item;

item store_weapons[] = { { "Dagger", 8, 4, 0 },
			 { "Shortsword", 10, 5, 0 },
			 { "Warhammer", 25, 6, 0 },
			 { "Longsword", 40, 7, 0 },
			 { "Greataxe", 74, 8, 0 } };

item store_armors[] = { { "Leather", 13, 0, 1 },
			{ "Chainmail", 31, 0, 2 },
			{ "Splintmail", 53, 0, 3 },
			{ "Bandedmail", 75, 0, 4 },
			{ "Platemail", 102, 0, 5 } };

item store_rings[] = { { "Damage +1", 25, 1, 0 },  { "Damage +2", 50, 2, 0 },
		       { "Damage +3", 100, 3, 0 }, { "Defense +1", 20, 0, 1 },
		       { "Defense +2", 40, 0, 2 }, { "Defense +3", 80, 0, 3 } };

typedef struct {
	const char *name;
	int armor_score;
	int damage_score;
	int hit_points;
	item weapon;
	item armor;
	item rings[2];
} character;

character boss = { .name = "boss", .armor_score = 2, .damage_score = 8, .hit_points = 109 };

character player = { .name = "player", .armor_score = 0, .damage_score = 0, .hit_points = 100 };

char comb_buffer[200]; // Used to save the combinations for a moment

void display_items(item items[], int count, const char *category);
void display_store_items();
void display_character_stats();
void perform_attack(character *attacker, character *defender);
void combinations(int v[], int start, int n, int k, int maxk);
void parse_combinations(int valid_combinations[MAX_COMBINATIONS][MAX_ELEMENTS_PER_COMBINATION]);
int run_game();
void reset_universe();

int main()
{
	// Mark all array elements as not used
	for (int i = 0; i < MAX_COMBINATIONS; i++) {
		for (int j = 0; j < MAX_ELEMENTS_PER_COMBINATION; j++) {
			weapon_combinations[i][j] = -1;
			armor_combinations[i][j] = -1;
			ring1_combinations[i][j] = -1;
			ring2_combinations[i][j] = -1;
		}
	}

	display_store_items();

	//  NOTE: Execute simulations for all combinations
	//  1 weapon for w -> wC1 = m = 5
	//  0-1 armor for a -> aC0 + aC1 = 1 + n = 6
	//  0-2 rings for r -> rC0 + rC1 + rC2 = 1 + r + rC2 = 7 + rC2 = 22
	//  Total combinations = wC1 * ( aC0 + aC1 ) * ( rC0 + rC1 + rC2 ) = 660

	int v[MAX_COMBINATIONS];

	// weapon wC1 combinations
	combinations(v, 1, 5, 1, 1);
	parse_combinations(weapon_combinations);

	// armor aC0 + aC1 combinations
	combinations(v, 0, 5, 1, 1);
	parse_combinations(armor_combinations);

	// ring rC0 + rC1 combinations
	combinations(v, 0, 6, 1, 1);
	parse_combinations(ring1_combinations);

	// ring rC2 combinations
	combinations(v, 0, 6, 1, 2);
	parse_combinations(ring2_combinations);

	unsigned int min_cost = 4e9, max_cost = 0;
	// Nested loops that generates all combinations
	for (size_t w = 0; w < MAX_COMBINATIONS && weapon_combinations[w][0] != -1; w++) {
		for (size_t a = 0; a < MAX_COMBINATIONS && armor_combinations[a][0] != -1; a++) {
			for (size_t r1 = 0;
			     r1 < MAX_COMBINATIONS && ring1_combinations[r1][0] != -1; r1++) {
				int w_idx = weapon_combinations[w][0] - 1; // [0, 4]
				int a_idx = armor_combinations[a][0] - 1; // [-1, 4]
				int r1_idx = ring1_combinations[r1][0] - 1; // [-1, 5]

				player.weapon = store_weapons[w_idx];
				player.damage_score += player.weapon.damage;
				unsigned int game_cost = store_weapons[w_idx].cost;
				if (a_idx != -1) {
					player.armor = store_armors[a_idx];
					player.armor_score += player.armor.armor;
					game_cost += store_armors[a_idx].cost;
				}
				if (r1_idx != -1) {
					player.rings[0] = store_rings[r1_idx];
					player.armor_score += player.rings[0].armor;
					player.damage_score += player.rings[0].damage;
					game_cost += store_rings[r1_idx].cost;
				}
				// printf("Weapon %d\t", weapon_combinations[w][0]);
				// printf("Armor %d\t", armor_combinations[a][0]);
				// printf("Ring1 %d\tRing2 X\n", ring1_combinations[r1][0]);
				// display_character_stats();
				if (run_game())
					min_cost = min(min_cost, game_cost);
				else
					max_cost = max(max_cost, game_cost);
				reset_universe();
			}

			for (size_t r2 = 0;
			     r2 < MAX_COMBINATIONS && ring2_combinations[r2][0] != -1; r2++) {
				int w_idx = weapon_combinations[w][0] - 1; // [0, 4]
				int a_idx = armor_combinations[a][0] - 1; // [-1, 4]
				int r2_idx0 = ring2_combinations[r2][0] - 1; // [-1, 5]
				int r2_idx1 = ring2_combinations[r2][1] - 1; // [-1, 5]

				player.weapon = store_weapons[w_idx];
				player.damage_score += player.weapon.damage;
				unsigned int game_cost = store_weapons[w_idx].cost;
				if (a_idx != -1) {
					player.armor = store_armors[a_idx];
					player.armor_score += player.armor.armor;
					game_cost += store_armors[a_idx].cost;
				}
				if (r2_idx0 != -1) {
					player.rings[0] = store_rings[r2_idx0];
					player.armor_score += player.rings[0].armor;
					player.damage_score += player.rings[0].damage;
					game_cost += store_rings[r2_idx0].cost;
				}
				if (r2_idx1 != -1) {
					player.rings[1] = store_rings[r2_idx1];
					player.armor_score += player.rings[1].armor;
					player.damage_score += player.rings[1].damage;
					game_cost += store_rings[r2_idx1].cost;
				}
				// printf("Weapon %d\t", weapon_combinations[w][0]);
				// printf("Armor %d\t", armor_combinations[a][0]);
				// printf("Ring1 %d\tRing2 %d\n", ring2_combinations[r2][0],
				//        ring2_combinations[r2][1]);
				// display_character_stats();
				if (run_game())
					min_cost = min(min_cost, game_cost);
				else
					max_cost = max(max_cost, game_cost);

				reset_universe();
			}
		}
	}
	printf("\nThe least amount of gold you can spend and still win the fight is %u.\n",
	       min_cost);
	printf("\nThe most amount of gold you can spend and still lose the fight is %u.\n",
	       max_cost);
	return 0;
}

/* Displays items from a single category. */
void display_items(item items[], int count, const char *category)
{
	printf("\033[1m%s:\n", category);
	printf("%-12s %-5s %-7s %-5s\n", "Name", "Cost", "Damage", "Armor\033[0m");
	for (int i = 0; i < count; i++) {
		printf("%-12s %-5d %-7d %-5d\n", items[i].name, items[i].cost, items[i].damage,
		       items[i].armor);
	}
	printf("\n");
}

/* Displays the items available in the store along their stats. */
void display_store_items()
{
	int weapon_ount = sizeof(store_weapons) / sizeof(store_weapons[0]);
	int armor_count = sizeof(store_armors) / sizeof(store_armors[0]);
	int ring_count = sizeof(store_rings) / sizeof(store_rings[0]);

	printf("\n < - - - - -  \033[1mSTORE\033[0m  - - - - - >\n\n");
	display_items(store_weapons, weapon_ount, "WEAPONS");
	display_items(store_armors, armor_count, "ARMOR");
	display_items(store_rings, ring_count, "RINGS");
	printf(" < - - - - - - - - - - - - - - >\n\n");
}

/* Displays player's current stats. */
void display_character_stats()
{
	printf("\033[1m\n%s's stats\033[0m\n", player.name);
	printf("Armor score: %10d\n", player.armor_score);
	printf("Damage score: %9d\n", player.damage_score);
	printf("Hit points: %11d\n", player.hit_points);
	printf("Weapon: %15s\n", player.weapon.name);
	printf("Armor: %16s\n", player.armor.name ? player.armor.name : "(null)");
	printf("Ring 0: %15s\n", player.rings[0].name ? player.rings[0].name : "(null)");
	printf("Ring 1: %15s\n\n", player.rings[1].name ? player.rings[1].name : "(null)");
}

/* Performs an attack using two characters. */
void perform_attack(character *attacker, character *defender)
{
	int attack = attacker->damage_score - defender->armor_score;
	defender->hit_points -= attack;
	// printf("The %s deals %d-%d=%d damage; ", attacker->name, attacker->damage_score,
	//        defender->armor_score, attack);
	// printf("the %s goes down to %d hit points.\n", defender->name, defender->hit_points);
}

/* Executes the turn-based game. */
int run_game()
{
	unsigned int turn = 0;
	do {
		if (turn % 2 == 0)
			perform_attack(&player, &boss);
		else
			perform_attack(&boss, &player);
		turn++;
	} while (player.hit_points > 0 && boss.hit_points > 0);

	return turn % 2;
}

/* Resets players' stats. */
void reset_universe()
{
	memset(&player, 0, sizeof(character));
	player.name = "player";
	player.hit_points = 100;
	boss.hit_points = 109;
}

/**
 * @brief Generates all combinations of integers from a given range.
 *
 * This function generates all combinations of integers from a given range [start, n]
 * and prints each combination of length `maxk`.
 *
 * @param v An array to hold the current combination. It should have a size of at least `maxk + 1`.
 * @param start The starting integer for the current combination. This determines the range of integers to use.
 * @param n The ending integer for the current combination. This determines the range of integers to use.
 * @param k The current position in the combination array `v` that is being filled.
 * @param maxk The length of the combinations to generate.
 *
 * The function works by recursively filling the combination array `v`. When `k` exceeds `maxk`,
 * it means a complete combination of length `maxk` has been generated, and the function prints it.
 *
 * Example usage:
 * @code
 * int v[4]; // Assuming we want combinations of length 3 (maxk = 3)
 * combinations(v, 1, 5, 1, 3); // Generates all combinations of 3 elements from the range [1, 5]
 * @endcode
 *
 * Combinations algorithm taken from:
 * https://people.engr.tamu.edu/djimenez/ut/utsa/cs3343/lecture25.html
 */
void combinations(int v[], int start, int n, int k, int maxk)
{
	int i;

	// k here counts through positions in the maxk-element v.
	// if k > maxk, then the v is complete and we can use it.
	if (k > maxk) {
		// Print data to temporary buffer
		for (i = 1; i <= maxk; i++)
			sprintf(comb_buffer + strlen(comb_buffer), "%i ", v[i]);
		sprintf(comb_buffer + strlen(comb_buffer), "\n");
		return;
	}

	// for this k'th element of the v, try all start..n
	// elements in that position
	for (i = start; i <= n; i++) {
		v[k] = i;
		// recursively generate combinations of integers
		//        from i+1..n
		combinations(v, i + 1, n, k + 1, maxk);
	}
}

/* Parses buffer to get the indexes of the valid combinations. */
void parse_combinations(int valid_combinations[MAX_COMBINATIONS][MAX_ELEMENTS_PER_COMBINATION])
{
	char *token;
	char *saveptr;
	int num_combinations = 0;

	token = strtok_r(comb_buffer, "\n", &saveptr);

	while (token != NULL && num_combinations < MAX_COMBINATIONS) {
		char *subtoken;
		char *subsaveptr;
		int num_elements = 0;

		subtoken = strtok_r(token, " ", &subsaveptr);

		while (subtoken != NULL && num_elements < MAX_ELEMENTS_PER_COMBINATION) {
			// Convert subtoken to integer (index) and store in combinations array
			valid_combinations[num_combinations][num_elements] = atoi(subtoken);
			// Get next index in the line
			subtoken = strtok_r(NULL, " ", &subsaveptr);

			num_elements++;
		}
		num_combinations++;
		// Get next line in the buffer
		token = strtok_r(NULL, "\n", &saveptr);
	}
	memset(comb_buffer, '\0', sizeof(comb_buffer)); // reset buffer, data already copied
}
