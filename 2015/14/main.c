/*
 * Advent of Code 2015
 * --- Day 14: Reindeer Olympics ---
 * https://adventofcode.com/2015/day/14
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

#define BUFF_SIZE	75
#define MAX_REINDEERS	9
#define SECONDS		2503
#define max(a,b)	((a) > (b)) ? a : b

struct reindeer {
	unsigned int d, v, Ton, Toff, p;
}r[MAX_REINDEERS];

int main()
{
	FILE *file;
	char buffer[BUFF_SIZE];
	unsigned int t = 0, z = 0;

	file = fopen("input", "r");
	while (fgets(buffer, sizeof(buffer), file) != NULL) {
		// Parse the line and extract the desired elements
		if (sscanf
		    (buffer, "%*s %*s %*s %d %*s %*s %d %*s %*s %*s %*s %*s %*s %d %*s", &r[z].v,
		     &r[z].Ton, &r[z].Toff) == 3)
			z++;
	}
	fclose(file);

	do {
		for (unsigned int i = 0; i < MAX_REINDEERS; i++)
			if (t % (r[i].Ton + r[i].Toff) < r[i].Ton)
				r[i].d += r[i].v;

		unsigned d_max = 0;
		for (unsigned int i = 0; i < MAX_REINDEERS; i++)
			d_max = max(d_max, r[i].d);

		for (unsigned int i = 0; i < MAX_REINDEERS; i++)
			if (r[i].d == d_max)
				r[i].p++;

		t++;
		/* printf("t=%d\t", t); */
		/* for (unsigned int i = 0; i < MAX_REINDEERS; i++) */
		/* 	printf("%d\t", r[i].d); */
		/* printf("%d\t", d_max); */
		/* for (unsigned int i = 0; i < MAX_REINDEERS; i++) */
		/* 	printf("%d\t", r[i].p); */
		/* printf("\n"); */
	} while (t < SECONDS);

	unsigned d_max = 0, p_max = 0;
	for (unsigned int i = 0; i < MAX_REINDEERS; i++) {
		d_max = max(d_max, r[i].d);
		p_max = max(p_max, r[i].p);
	}
	printf("Maximum distance is: %d\n", d_max);
	printf("Maximum score is: %d\n", p_max);
	return 0;
}
