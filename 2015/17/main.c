/*
 * Advent of Code 2015
 * --- Day 17: No Such Thing as Too Much ---
 * https://adventofcode.com/2015/day/17
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

#define LITERS 150
#define CONTAINERS 20

void bubble_sort(unsigned int arr[], unsigned int size);
void brute_force_comb(unsigned int min, unsigned int max);
void combinations(int v[], int start, int n, int k, int maxk);

unsigned int comb_total, comb_local, containers[CONTAINERS];

int main()
{
	FILE *file;
	unsigned int i = 0;

	file = fopen("input", "r");
	while (fscanf(file, "%d", &containers[i]) == 1)
		i++;
	fclose(file);

	bubble_sort(containers, CONTAINERS);

	unsigned int min_c = 0, min = 0, max_c = 0, max = 0;
	for (size_t i = 0; i < CONTAINERS; i++) {
		if (min_c < LITERS) {
			min_c += containers[CONTAINERS - i - 1];
			min++;
		}
		if (max_c < LITERS) {
			max_c += containers[i];
			max++;
		}
	}

	brute_force_comb(min, max);

	return 0;
}

/* Implementation of bubble sort algorithm */
void bubble_sort(unsigned int arr[], unsigned int size)
{
	int i, j, temp;
	for (i = 0; i < size - 1; i++) {
		for (j = 0; j < size - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				// Swap the elements if they are in the wrong order
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

/* Brute force, check all the combinations */
void brute_force_comb(unsigned int min, unsigned int max)
{
	// Since we don't know how many containers are needed
	// let's try all combinations of "number of containers"
	// that can sum 150 but instead of running from 0 to
	// CONTAINERS let's run from min to max.
	// Then let's try all combinations of taking k elements
	// from the original CONTAINERS elements i.e. nCk
	for (unsigned int k = min; k <= max; ++k) {
		int arr[k]; // Contains the indexes
		combinations(arr, 0, CONTAINERS, 0, k);
		printf("\nThere are %d combinations with %d containers\n", comb_local, k);
		comb_local = 0;
	}
	printf("\nThere are %d combinations in total\n", comb_total);
}

/* Combinations algorithm taken from: */
/* https://people.engr.tamu.edu/djimenez/ut/utsa/cs3343/lecture25.html */
void combinations(int v[], int start, int n, int k, int maxk)
{
	unsigned int i, liters = 0;
	// k here counts through positions in the maxk-element v.
	// if k > maxk, then the v is complete and we can use it.
	if (k == maxk) {
		for (i = 0; i < maxk; i++)
			liters += containers[v[i]];
		if (liters == 150) {
			/* for (size_t i = 0; i < maxk; i++) */
			/*      printf("%d,%d ", v[i], containers[v[i]]); */
			/* printf("\n"); */
			comb_total++;
			comb_local++;
		}
		return;
	}
	// for this k'th element of the v, try all start..n
	// elements in that position
	for (i = start; i < n; i++) {
		v[k] = i;
		// recursively generate combinations of integers from i+1..n
		combinations(v, i + 1, n, k + 1, maxk);
	}
}
