/*
 * Advent of Code 2015
 * ---- Day 18: Like a GIF For Your Yard ---
 * https://adventofcode.com/2015/day/18
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
/* #include <stdlib.h> */
#include <unistd.h>

#define ROWS		100
#define COLS		100
#define STEPS		100
#define CONWAY_MOD
#define ANIMATION
#define max(a,b) (((a) > (b)) ? (a) : (b))
#define min(a,b) (((a) < (b)) ? (a) : (b))

unsigned char lights[ROWS][COLS];

void update_state();
void show_lights();
unsigned int lights_on();

int main()
{
	FILE *file;
	char c;

	file = fopen("input", "r");
	for (size_t row = 0; row < ROWS; row++) {
		for (size_t col = 0; col < COLS; col++) {
			fscanf(file, "%c", &c);
			if (c == '.')
				lights[row][col] = 0;
			else
				lights[row][col] = 1;
		}
		fscanf(file, "\n");
	}
	fclose(file);

#ifdef CONWAY_MOD
	lights[0][0] = 1;
	lights[0][COLS - 1] = 1;
	lights[COLS - 1][0] = 1;
	lights[COLS - 1][COLS - 1] = 1;
#endif				/* ifdef CONWAY_MOD */

#ifdef ANIMATION
	show_lights();
	for (size_t i = 0; i < STEPS; i++) {
		printf("\e[1;1H\e[2J");	// Clear screen, simpler
		/* system("clear"); // Uses <stdlib.h>, cool but creates another process */

		update_state();
		show_lights();

		/* sleep(1); // Uses <unistd.h> */
		usleep(400000);	// Uses <unistd.h>
	}
#else
	for (size_t i = 0; i < STEPS; i++)
		update_state();

	show_lights();

	printf("There are %u lights on\n", lights_on());
#endif				/* ifdef ANIMATION */

	return 0;
}

/* Change state of lights */
void update_state()
{
	for (int row = 0; row < ROWS; row++) {
		for (int col = 0; col < COLS; col++) {
			// Count neighbors
			int neighbors = 0;
			for (int r = max(0, (row - 1)); r <= min((row + 1), (COLS - 1)); r++)
				for (int c = max(0, col - 1); c <= min(col + 1, COLS - 1); c++)
					if (!(r == row && c == col) && lights[r][c] & 0x01)
						neighbors++;
			// Apply rules
			if (lights[row][col] & 0x01) {
				if (neighbors == 2 || neighbors == 3)
					lights[row][col] |= 1 << 1;
			} else {
				if (neighbors == 3)
					lights[row][col] |= 1 << 1;
			}
		}
	}
	// Update states
	for (size_t row = 0; row < ROWS; row++)
		for (size_t col = 0; col < COLS; col++)
			lights[row][col] >>= 1;
#ifdef CONWAY_MOD
	lights[0][0] = 1;
	lights[0][COLS - 1] = 1;
	lights[COLS - 1][0] = 1;
	lights[COLS - 1][COLS - 1] = 1;
#endif				/* ifdef CONWAY_MOD */
}

/* Print the current state of the system */
void show_lights()
{
	for (size_t row = 0; row < ROWS; row++) {
		for (size_t col = 0; col < COLS; col++)
			if (lights[row][col])
				printf("#");
			else
				printf("·");
		printf("\n");
	}
}

/* Returns the number of on lights */
unsigned int lights_on()
{
	unsigned int on = 0;
	for (int row = 0; row < ROWS; row++)
		for (int col = 0; col < COLS; col++)
			on += lights[row][col];
	return on;
}
