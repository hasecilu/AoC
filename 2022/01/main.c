/*
 * Advent of Code 2022
 * --- Day 1: Calorie Counting ---
 * https://adventofcode.com/2022/day/1
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFF_SIZE   8
unsigned long int calSum[250], maximum[3], total = 0, reindeer = 0, pos[3];

int main()
{
	FILE *p;
	char str[BUFF_SIZE];
	p = fopen("data", "r");

	while (fgets(str, BUFF_SIZE, p) != NULL) {
		if (strlen(str) == 1) {
			if (calSum[reindeer] > maximum[0]) {
				maximum[2] = maximum[1];
				maximum[1] = maximum[0];
				maximum[0] = calSum[reindeer];
				pos[0] = reindeer;
			} else if (calSum[reindeer] > maximum[1]) {
				maximum[2] = maximum[1];
				maximum[1] = calSum[reindeer];
				pos[1] = reindeer;
			} else if (calSum[reindeer] > maximum[2]) {
				maximum[2] = calSum[reindeer];
				pos[2] = reindeer;
			}
			reindeer++;
		} else
			calSum[reindeer] += atol(str);
	}

	fclose(p);

	for (int i = 0; i < 3; i++) {
		printf("\nReindeer #%lu carry %lu cal", pos[i] + 1, maximum[i]);
		total += maximum[i];
	}
	printf("\n\nTotal is %lu cal\n", total);

	return 0;
}
