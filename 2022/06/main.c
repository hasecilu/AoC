/*
 * Advent of Code 2022
 * --- Day 6: Tuning Trouble ---
 * https://adventofcode.com/2022/day/6
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

#define MARKER_POS 14

int main()
{
	FILE *file;
	char ch = '\0';
	unsigned char buff[MARKER_POS], s = 0;
	unsigned int pos = MARKER_POS;

	file = fopen("data", "r");
	for (unsigned char i = 0; i < MARKER_POS - 1; i++)
		buff[i] = fgetc(file);
	do {
		buff[MARKER_POS - 1] = fgetc(file);

		for (unsigned char i = 0; i < MARKER_POS; i++)
			for (unsigned char j = 0; j < MARKER_POS; j++)
				if (i != j && buff[i] == buff[j])
					s++;

		if (s > 0) {
			for (unsigned char i = 0; i < MARKER_POS - 1; i++)
				buff[i] = buff[i + 1];
			pos++;
			s = 0;
		} else
			break;
	} while (ch != EOF);
	fclose(file);

	printf("%u characters processed\n", pos);

	return 0;
}
