/*
 * Advent of Code 2022
 * --- Day 9: Rope Bridge ---
 * https://adventofcode.com/2022/day/9
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>

#define SAVE_TO_FILE
#define ROPE_LENGHT	10
#define increment(d)	((d) > 0) ? 1 : -1

struct knot {
	int x[56000], y[56000];
};

void move_head(char direction, unsigned int value);
void move_subtail();
void new_t();
void print2file();
int visited_positions(struct knot A);

struct knot rope[ROPE_LENGHT];
unsigned int t = 0;

int main()
{
	FILE *file;
	char direction;
	int value;
	file = fopen("input", "r");

	while (fscanf(file, " %c %d", &direction, &value) == 2)
		move_head(direction, value);

	fclose(file);

	printf("The T knot visited %d positions\n", visited_positions(rope[ROPE_LENGHT-1]));

	return 0;
}

/* Move head knot by "value" units */
void move_head(char direction, unsigned int value)
{
	while (value) {
		new_t();

		switch (direction) {
		case 'U':
			rope[0].y[t]++;
			break;
		case 'D':
			rope[0].y[t]--;
			break;
		case 'L':
			rope[0].x[t]--;
			break;
		case 'R':
			rope[0].x[t]++;
			break;
		}
#ifdef SAVE_TO_FILE
		print2file();
#endif				/* ifdef SAVE_TO_FILE */
		move_subtail();
		value--;
	}
}

/* Move (sub)tail knots, if necessary */
void move_subtail()
{
	for (unsigned char i = 0; i < ROPE_LENGHT - 1; i++) {
		// rope[i] is subhead knot
		// rope[i+1] is subtail knot
		int dx = rope[i].x[t] - rope[i + 1].x[t];
		int dy = rope[i].y[t] - rope[i + 1].y[t];

		if (abs(dx) > 1 || abs(dy) > 1) {
			new_t();

			if (dx == 0) {		// Case 0: Ahead knot is up or down
				rope[i + 1].y[t] += increment(dy);
			} else if (dy == 0) {	// Case 1: Ahead knot is left or right
				rope[i + 1].x[t] += increment(dx);
			} else {		// Case 2: Ahead knot is in diagonal
				rope[i + 1].x[t] += increment(dx);
				rope[i + 1].y[t] += increment(dy);
			}
			// OLD CODE
			// Move subtail to subhead pos at t-2
			// Next 2 lines worked with 2knots case but didn't
			// worked with 10 knots because is a rope not a snake
			/* rope[i + 1].x[t] = rope[i].x[t - 2]; */
			/* rope[i + 1].y[t] = rope[i].y[t - 2]; */
#ifdef SAVE_TO_FILE
			print2file();
#endif				/* ifdef SAVE_TO_FILE */
		}
	}
}

/* Create new time sample, copy values from t-1 */
void new_t()
{
	t++;
	for (unsigned char i = 0; i < ROPE_LENGHT; i++) {
		rope[i].x[t] = rope[i].x[t - 1];
		rope[i].y[t] = rope[i].y[t - 1];
	}
}

/* Print to "data" file all (x,y) pairs, a single line for a single t moment*/
void print2file()
{
	FILE *fp;
	fp = fopen("data", "a");
	for (unsigned char i = 0; i < ROPE_LENGHT; i++) {
		fprintf(fp, "%d %d ", rope[i].x[t], rope[i].y[t]);
	}
	fprintf(fp, "\n");
	fclose(fp);
}

/* Check for unique visited coordinated pairs for a given knot */
int visited_positions(struct knot A)
{
	unsigned int positions = 0;
	for (unsigned int i = 0; i <= t; i++) {
		unsigned char unique = 1;
		for (unsigned int j = i + 1; j <= t; j++) {
			if (A.x[i] == A.x[j] && A.y[i] == A.y[j]) {
				unique = 0;
				break;
			}
		}
		if (unique)
			positions++;
	}
	return positions;
}
