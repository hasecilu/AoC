/*
 * Advent of Code 2022
 * --- Day 3: Rucksack Reorganization ---
 * https://adventofcode.com/2022/day/3
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <string.h>

#define	BUFF_SIZE	50

unsigned long int prio[300], line = 0, sum = 0, sumG = 0;
char len[3];

int main()
{
	FILE *p;
	char str[BUFF_SIZE], s[3][BUFF_SIZE];
	p = fopen("data", "r");

	while ((fgets(str, BUFF_SIZE, p)) != NULL) {
		len[line % 3] = strlen(str) - 1;
		strcpy(s[line % 3], str);

		for (int i = 0; i < len[line % 3] / 2; i++) {
			for (int j = len[line % 3] / 2; j < len[line % 3]; j++) {
				if (str[i] == str[j]) {
					if (str[i] >= 97)
						prio[line] += str[i] - 96;
					else
						prio[line] += str[i] - 38;
					sum += prio[line];
					//printf("Line %lu -> %c -> %lu\n", line, str[i], prio[line]);

					if (line % 3 == 2) {
						for (int k = 0; k < len[0]; k++) {
							for (int l = 0; l < len[1]; l++) {
								for (int m = 0; m < len[2]; m++) {
									if (s[0][k] == s[1][l] && s[0][k] == s[2][m]) {
										if (s[0][k] >= 97)
											sumG += s[0][k] - 96;
										else
											sumG += s[0][k] - 38;
										//printf("Group %d -> %c -> %lu\n", line/3, s[0][k], sumG);
										goto sk;
									}
								}
							}
						}
					}
 sk:
					line++;
					goto skip;
				}
			}
		}
 skip:
	}

	fclose(p);

	printf("\nPriorities by rucksack: %lu\n", sum);
	printf("\nPriorities by group: %lu\n", sumG);

	return 0;
}
