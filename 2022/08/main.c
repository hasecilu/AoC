/*
 * Advent of Code 2022 
 * --- Day 8: Treetop Tree House ---
 * https://adventofcode.com/2022/day/8
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

#define GRID_SIZE 99

struct tree {
	int height[GRID_SIZE][GRID_SIZE];
	int is_visible[GRID_SIZE][GRID_SIZE];
	int scenic_score[GRID_SIZE][GRID_SIZE];
} grid;

void read_grid();
void print_grid();
void visibility_test();
int visible(unsigned char r, unsigned char c);
int v_top(unsigned char row, unsigned char col);
int v_bottom(unsigned char row, unsigned char col);
int v_left(unsigned char row, unsigned char col);
int v_right(unsigned char row, unsigned char col);
void scenic_score_test();
int score(unsigned char r, unsigned char c);
int ss_top(unsigned char row, unsigned char col);
int ss_bottom(unsigned char row, unsigned char col);
int ss_left(unsigned char row, unsigned char col);
int ss_right(unsigned char row, unsigned char col);

int main()
{
	read_grid();
	visibility_test();
	scenic_score_test();
	print_grid();
	return 0;
}

void read_grid()
{
	int temp[GRID_SIZE][GRID_SIZE];
	FILE *file = fopen("input", "r");
	for (unsigned char row = 0; row < GRID_SIZE; row++) {
		for (unsigned char col = 0; col < GRID_SIZE; col++) {
			fscanf(file, "%1d", &temp[row][col]);
			grid.height[row][col] = temp[row][col];
			grid.is_visible[row][col] = 1;
		}
	}
	fclose(file);
}

void print_grid()
{
	for (unsigned char row = 0; row < GRID_SIZE; row++) {
		for (unsigned char col = 0; col < GRID_SIZE; col++)
			if (grid.is_visible[row][col])
				printf("%d", grid.height[row][col]);
			else
				printf(" ");
		printf("\n");
	}
}

void visibility_test()
{
	int visible_trees = 4 * (GRID_SIZE - 1);
	for (unsigned char row = 1; row < GRID_SIZE - 1; row++) {
		for (unsigned char col = 1; col < GRID_SIZE - 1; col++) {
			grid.is_visible[row][col] = visible(row, col);
			visible_trees += grid.is_visible[row][col];
		}
	}
	printf("\nThere are %d visible trees\n\n", visible_trees);
}

int visible(unsigned char r, unsigned char c)
{
	return (v_top(r, c) | v_bottom(r, c) | v_left(r, c) | v_right(r, c));
}

int v_top(unsigned char row, unsigned char col)
{
	for (unsigned char i = 0; i < row; i++)
		if (grid.height[i][col] >= grid.height[row][col])
			return 0;
	return 1;
}

int v_bottom(unsigned char row, unsigned char col)
{
	for (unsigned char i = row + 1; i < GRID_SIZE; i++)
		if (grid.height[i][col] >= grid.height[row][col])
			return 0;
	return 1;
}

int v_left(unsigned char row, unsigned char col)
{
	for (unsigned char i = 0; i < col; i++)
		if (grid.height[row][i] >= grid.height[row][col])
			return 0;
	return 1;
}

int v_right(unsigned char row, unsigned char col)
{
	for (unsigned char i = col + 1; i < GRID_SIZE; i++)
		if (grid.height[row][i] >= grid.height[row][col])
			return 0;
	return 1;
}

void scenic_score_test()
{
	int highest_ss = 0, r = 0, c = 0;
	for (unsigned char row = 0; row < GRID_SIZE; row++) {
		for (unsigned char col = 0; col < GRID_SIZE; col++) {
			grid.scenic_score[row][col] = score(row, col);
			if (grid.scenic_score[row][col] > highest_ss) {
				highest_ss = grid.scenic_score[row][col];
				r = row;
				c = col;
			}
		}
	}
	printf("Highest scenic score %d \n\n", grid.scenic_score[r][c]);
}

int score(unsigned char r, unsigned char c)
{
	return ss_top(r, c) * ss_bottom(r, c) * ss_left(r, c) * ss_right(r, c);
}

int ss_top(unsigned char row, unsigned char col)
{
	if (row == 0)
		return 0;
	else if (row == 1)
		return 1;
	int ss = 0;
	for (unsigned char i = row - 1; i > 0 && grid.height[i][col] < grid.height[row][col]; i--)
		ss++;
	return ++ss;
}

int ss_bottom(unsigned char row, unsigned char col)
{
	if (row == GRID_SIZE - 1)
		return 0;
	else if (row == GRID_SIZE - 2)
		return 1;
	int ss = 0;
	for (unsigned char i = row + 1;
	     i < GRID_SIZE - 1 && grid.height[i][col] < grid.height[row][col]; i++)
		ss++;
	return ++ss;
}

int ss_left(unsigned char row, unsigned char col)
{
	if (col == 0)
		return 0;
	else if (col == 1)
		return 1;
	int ss = 0;
	for (unsigned char i = col - 1; i > 0 && grid.height[row][i] < grid.height[row][col]; i--)
		ss++;
	return ++ss;
}

int ss_right(unsigned char row, unsigned char col)
{
	if (col == GRID_SIZE - 1)
		return 0;
	else if (col == GRID_SIZE - 2)
		return 1;
	int ss = 0;
	for (unsigned char i = col + 1;
	     i < GRID_SIZE - 1 && grid.height[row][i] < grid.height[row][col]; i++)
		ss++;
	return ++ss;
}
