/*
 * Advent of Code 2022
 * --- Day 5: Supply Stacks ---
 * https://adventofcode.com/2022/day/5
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define       DEBUG
#define STR_BUFF_SIZE 37
#define CRATE_MOVER 9001 //9000 or 9001

char spc[6], n[4][3];
unsigned char num[3], last[9];
unsigned int crateD = 0;

void spaces_pos(char *s, char *sp);

int main()
{
	FILE *p;
	char str[STR_BUFF_SIZE];
	p = fopen("data", "r");
	char crates[60][9];
	for (size_t i = 0; i < 50; i++) {
		for (size_t j = 0; j < 50; j++)
			crates[i][j] = ' ';
	}

	while ((fgets(str, STR_BUFF_SIZE, p)) != NULL) {
		if (crateD < 8) { // 8 rows
			for (size_t i = 0; i < 9; i++) { // 9 columns
				crates[7 - crateD][i] = str[4 * i + 1];
				if (crates[7 - crateD][i] != ' ') {
					if (7 - crateD > last[i])
						last[i] = 7 - crateD;
				}
			}
		} else {
			if (str[0] == 'm') {
				spaces_pos(str, spc);
				for (size_t i = 0; i < 3; i++) { // get nunmbers
					size_t jj = 0;
					for (size_t j = spc[2 * i]; j < spc[2 * i + 1]; j++) {
						n[i][jj] = str[j];
						jj++;
					}
					num[i] = atoi(n[i]);
				}
				num[1]--; // 1-9 -> 0-8
				num[2]--;
#ifdef DEBUG
				printf("%u: %d,%d,%d\n", crateD - 9, num[0], num[1] + 1,
				       num[2] + 1);
#endif
#if CRATE_MOVER == 9000
				for (size_t move = 0; move < num[0];
				     move++) { // make moves of nth instruction
#ifdef DEBUG
					for (size_t i = 0; i < 9; i++)
						printf("%c ", crates[last[i]][i]);
					printf("\t->\t");
					printf("'%c'\t", crates[last[num[1]]][num[1]]);
					printf("'%c'\t", crates[last[num[2]]][num[2]]);
#endif
					if (crates[last[num[1]]][num[1]] != ' ') {
						crates[last[num[2]] + 1][num[2]] =
							crates[last[num[1]]][num[1]];
						crates[last[num[1]]][num[1]] = ' ';
						if (last[num[1]] != 0) {
							last[num[1]]--;
							last[num[2]]++;
						} else {
							last[num[2]]++;
						}
					}
#ifdef DEBUG
					for (size_t i = 0; i < 9; i++)
						printf("%c ", crates[last[i]][i]);
					printf("\t");
					for (size_t i = 0; i < 9; i++)
						printf("%d ", last[i]);
					printf("\n");
#endif
				}
#elif CRATE_MOVER == 9001
				// minor bug: skips position 0 when pile is empty
				while (num[0] - 1 > last[num[1]])
					num[0]--;
				for (size_t move = 0; move < num[0];
				     move++) { // make moves of nth instruction
					crates[last[num[2]] + 1 + move][num[2]] =
						crates[last[num[1]] - num[0] + 1 + move][num[1]];
					crates[last[num[1]] - num[0] + 1 + move][num[1]] = ' ';
				}
#ifdef DEBUG
				char max = last[0];
				for (size_t i = 1; i < 9; i++)
					max = (last[i] > max) ? last[i] : max;
				for (size_t row = max; row >= 0; row--) {
					for (size_t i = 0; i < 9; i++) {
						if (crates[row][i] != ' ')
							printf("[%c] ", crates[row][i]);
						else
							printf("    ");
					}
					printf("\n");
				}
				printf(" 1   2   3   4   5   6   7   8   9\n\n");
#endif
				//last[num[1]]-=num[0];
				last[num[1]] = (last[num[1]] - num[0]) > 0 ? last[num[1]] - num[0] :
									     0;
				last[num[2]] += num[0];
#endif
				for (size_t i = 0; i < 4; i++) { // clean number strings
					for (size_t j = 0; j < 3; j++)
						n[i][j] = '\0';
				}
			}
		}
		crateD++;
	}

	fclose(p);

	printf("\n\nUp crates are: ");
	for (size_t i = 0; i < 9; i++)
		printf("%c", crates[last[i]][i]);

	printf("\n");
	return 0;
}

void spaces_pos(char *s, char *sp)
{
	size_t pos = 0;
	for (int i = 0; i < strlen(s) - 1; i++) {
		if (s[i] == ' ') {
			sp[pos] = i;
			pos++;
		}
	}
	sp[5] = strlen(s) - 1;
}
