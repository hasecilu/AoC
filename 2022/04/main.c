/*
 * Advent of Code 2022
 * --- Day 4: Camp Cleanup ---
 * https://adventofcode.com/2022/day/4
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFF_SIZE 15

unsigned long int sum0 = 0, sum1 = 0;
char len = 0, buff[6], buff2[7], b0[4], b1[4], b2[4], b3[4], num[4];

void divide_by_char(char c, char *s, char *s1, char *s2);

int main()
{
	FILE *p;
	char str[BUFF_SIZE];
	p = fopen("data", "r");

	while ((fgets(str, BUFF_SIZE, p)) != NULL) {
		divide_by_char(',', str, buff, buff2);
		divide_by_char('-', buff, b0, b1);
		divide_by_char('-', buff2, b2, b3);

		// num[0]-num[1],num[2]-num[3]
		num[0] = atoi(b0);
		num[1] = atoi(b1);
		num[2] = atoi(b2);
		num[3] = atoi(b3);

		if ((num[0] <= num[2] && num[1]) >= num[3] ||
		    (num[0] >= num[2] && num[1] <= num[3]))
			sum0++;

		if (!((num[0] < num[2] && num[1] < num[2]) || (num[0] > num[3] && num[1] > num[3])))
			sum1++;

		memset(buff, 0, 6);
		memset(buff2, 0, 6);
		memset(b0, 0, 3);
		memset(b1, 0, 3);
		memset(b2, 0, 3);
		memset(b3, 0, 3);
	}

	fclose(p);

	printf("\n%lu ranges fully contain the other\n", sum0);
	printf("\n%lu ranges overlap\n", sum1);

	return 0;
}

void divide_by_char(char c, char *s, char *s1, char *s2)
{
	for (int i = 0; i < strlen(s) - 1; i++) {
		if (s[i] == c) {
			for (int j = 0; j < i; j++)
				s1[j] = s[j];
			for (int j = 0; j < strlen(s) - 1 - i; j++)
				s2[j] = s[j + i + 1];
		}
	}
	//printf("1: %s\n", s1);
	//printf("2: %s\n\n", s2);
}
