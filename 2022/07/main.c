/*
 * Advent of Code 2022 
 * --- Day 7: No Space Left On Device ---
 * https://adventofcode.com/2022/day/7
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define	STR_BUFF_SIZE   25
#define	FILENAME_SIZE   15
#define SIZE_LIMIT      (unsigned int)1e5
#define FS_LIMIT  	(unsigned int)7e7
#define SPACE4UPDATE  	(unsigned int)3e7
#define ARRAY_SIZE	10

struct node {
	char name[FILENAME_SIZE];
	char is_dir;
	unsigned int size;
	struct node *child, *sibling, *parent;
};

void find_character(char c, char *s, unsigned char *pos);
int str2int(char *str);
struct node *create_node(const char *name, char is_dir, int size);
void add_child(struct node *parent, struct node *child);
unsigned int get_size(struct node *root);
struct node *parse();
void print_tree(struct node *root, int depth);
void tree_traversal_part1(struct node *root);
void tree_traversal_part2(struct node *root, unsigned int missing_space);
unsigned int get_smallest(unsigned int *array);

unsigned int sizes_sum = 0, directories_sizes[10], pos;

int main()
{
	for (unsigned char i = 0; i < ARRAY_SIZE; i++)
		directories_sizes[i] = -1;

	// Generate filesystem tree
	struct node *root = parse();

	// Print tree including sizes
	print_tree(root, 0);

	tree_traversal_part1(root);
	printf("\nThe sum of directories smaller than size limit is: %d\n\n", sizes_sum);

	printf("Filesystem limit: %u\n", FS_LIMIT);
	printf("Used space: %u\n", get_size(root));
	printf("Needed space: %u\n", SPACE4UPDATE);
	printf("Free space: %u\n", FS_LIMIT - get_size(root));
	if (FS_LIMIT - get_size(root) < SPACE4UPDATE) {
		unsigned int missing_space = SPACE4UPDATE - (FS_LIMIT - get_size(root));
		printf("Missing space: %u\n\n", missing_space);
		tree_traversal_part2(root, missing_space);
		printf("The smallest directory size is: %u\n", get_smallest(directories_sizes));
	} else
		printf("Free space missing: 0\n");

	return 0;
}

/* Find character c on string s and return positions as an array */
void find_character(char c, char *s, unsigned char *pos)
{
	unsigned char index = 0;

	for (unsigned char i = 0; i < strlen(s) - 1; i++)
		if (s[i] == c)
			pos[index++] = i;
}

/* Convert string to integer */
int str2int(char *str)
{
	unsigned int res = 0;

	for (int i = 0; str[i] != '\0'; ++i)
		res = res * 10 + str[i] - '0';

	return res;
}

/* Create a new tree node */
struct node *create_node(const char *name, char is_dir, int size)
{
	struct node *new_node = (struct node *)malloc(sizeof(struct node));
	strcpy(new_node->name, name);
	new_node->is_dir = is_dir;
	new_node->size = size;
	new_node->child = NULL;
	new_node->sibling = NULL;
	new_node->parent = NULL;
	return new_node;
}

/* Function to add a child node to a parent node */
void add_child(struct node *parent, struct node *child)
{
	if (parent->child == NULL) {
		parent->child = child;
	} else {
		struct node *current = parent->child;
		while (current->sibling != NULL) {
			current = current->sibling;
		}
		current->sibling = child;
	}
}

/* Recursively get the size of a directory */
unsigned int get_size(struct node *root)
{
	if (root == NULL)
		return 0;

	// Only calculate if value is 0
	if (root->is_dir && !root->size) {
		int size = 0;
		struct node *current = root->child;
		while (current != NULL) {
			size += get_size(current);
			current = current->sibling;
		}
		root->size = size;
	}
	return root->size;
}

/* Parse the data and create filesystem tree. */
struct node *parse()
{
	FILE *p;
	p = fopen("data", "r");

	// The first node is root: '/'
	struct node *root = create_node("/", 1, 0);
	struct node *current = root;

	char buffer[STR_BUFF_SIZE];

	while ((fgets(buffer, STR_BUFF_SIZE, p)) != NULL) {
		unsigned char spaces[4] = { };
		find_character(' ', buffer, spaces);

		if (buffer[0] == '$' && buffer[2] == 'c') {
			if (buffer[5] == '/') {					// cd /
				// current node is already at root
			} else if (buffer[5] == '.' && buffer[6] == '.') {	// cd ..
				current = current->parent;
			} else {						// cd directory
				char directory[FILENAME_SIZE] = "";
				strncpy(directory, buffer + spaces[1] + 1, strlen(buffer) - 5 - 1);
				// Look for the directory on the children, 100% exist
				struct node *temp = current->child;
				while (temp != NULL) {
					int res = strcmp(directory, temp->name);
					if (!res) 
						break;
					temp = temp->sibling;
				}
				current = temp;
			}
		} else if (buffer[0] == '$' && buffer[2] == 'l') {		// ls
			// Dummy entry
		} else if (buffer[0] == 'd') {					// dir directory
			char directory[FILENAME_SIZE] = "";
			strncpy(directory, buffer + spaces[0] + 1, (strlen(buffer) - 4 - 1));
			struct node *temp = create_node(directory, 1, 0);
			temp->parent = current;
			add_child(current, temp);
		} else {							// 42069 filename
			char size_string[10] = "";
			strncpy(size_string, buffer, spaces[0]);
			unsigned int size = str2int(size_string);
			char filename[FILENAME_SIZE] = "";
			strncpy(filename, buffer + spaces[0] + 1, strlen(buffer) - strlen(size_string) - 2);
			struct node *temp = create_node(filename, 0, size);
			temp->parent = current;
			add_child(current, temp);
		}
	}

	fclose(p);

	return root;
}

/* Print tree structure */
void print_tree(struct node *root, int depth)
{
	for (int i = 0; i < depth; i++)
		printf("  ");

	printf("- %s (", root->name);
	if (root->is_dir)
		printf("dir");
	else
		printf("file");
	printf(", size=%d)\n", get_size(root));

	struct node *temp = root->child;
	while (temp != NULL) {
		print_tree(temp, depth + 1);
		temp = temp->sibling;
	}
}

/* Traverse the tree structure and find directories smaller than SIZE_LIMIT */
void tree_traversal_part1(struct node *root)
{
	if (root->is_dir && get_size(root) < SIZE_LIMIT)
		sizes_sum += get_size(root);

	struct node *temp = root->child;
	while (temp != NULL) {
		tree_traversal_part1(temp);
		temp = temp->sibling;
	}
}

/* Traverse the tree structure and find directories greater than missing_space */
void tree_traversal_part2(struct node *root, unsigned int missing_space)
{
	if (root->is_dir && get_size(root) >= missing_space)
		directories_sizes[pos++] = get_size(root);

	struct node *temp = root->child;
	while (temp != NULL) {
		tree_traversal_part2(temp, missing_space);
		temp = temp->sibling;
	}
}

/* Get the smallest value from an array */
unsigned int get_smallest(unsigned int array[])
{
	int smallest = array[0];

	for (unsigned char i = 0; i < ARRAY_SIZE; i++)
		if (array[i] < smallest)
			smallest = array[i];

	return smallest;
}
