/*
 * Advent of Code 2022
 * --- Day 2: Rock Paper Scissors ---
 * https://adventofcode.com/2022/day/2
 * --------------------
 * Author: @hasecilu
 */

#include <stdio.h>

#define	BUFF_SIZE	5

unsigned long int points[2];

int main()
{
	FILE *p;
	char str[BUFF_SIZE];
	p = fopen("data", "r");

	while (fgets(str, BUFF_SIZE, p) != NULL) {
		if (str[2] == 'X')
			points[0]++;
		else if (str[2] == 'Y') {
			points[0] += 2;
			points[1] += 3;
		} else if (str[2] == 'Z') {
			points[0] += 3;
			points[1] += 6;
		}

		if ((str[0] == 'A' && str[2] == 'X')
		    || (str[0] == 'B' && str[2] == 'Y')
		    || (str[0] == 'C' && str[2] == 'Z'))
			points[0] += 3;
		if ((str[0] == 'A' && str[2] == 'Y')
		    || (str[0] == 'B' && str[2] == 'Z')
		    || (str[0] == 'C' && str[2] == 'X'))
			points[0] += 6;

		if ((str[0] == 'A' && str[2] == 'X')
		    || (str[0] == 'B' && str[2] == 'Z')
		    || (str[0] == 'C' && str[2] == 'Y'))
			points[1] += 3;
		if ((str[0] == 'A' && str[2] == 'Z')
		    || (str[0] == 'B' && str[2] == 'Y')
		    || (str[0] == 'C' && str[2] == 'X'))
			points[1] += 2;
		if ((str[0] == 'A' && str[2] == 'Y')
		    || (str[0] == 'B' && str[2] == 'X')
		    || (str[0] == 'C' && str[2] == 'Z'))
			points[1]++;
	}

	fclose(p);

	printf("Part 1: You have %lu points\n", points[0]);
	printf("Part 2: You have %lu points\n", points[1]);

	return 0;
}
