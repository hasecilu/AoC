# Advent of Code 2022

| Day | Problem                         | Part 1 | Part 2 |
| :-: | :------------------------------ | :----: | :----: |
| 01  | [Calorie Counting][01]          |   ✔️    |   ✔️    |
| 02  | [Rock Paper Scissors][02]       |   ✔️    |   ✔️    |
| 03  | [Rucksack Reorganization][03]   |   ✔️    |   ✔️    |
| 04  | [Camp Cleanup][04]              |   ✔️    |   ✔️    |
| 05  | [Supply Stacks][05]             |   ✔️    |   ✔️    |
| 06  | [Tuning Trouble][06]            |   ✔️    |   ✔️    |
| 07  | [No Space Left On Device][07]   |   ✔️    |   ✔️    |
| 08  | [Treetop Tree House][08]        |   ✔️    |   ✔️    |
| 09  | [Rope Bridge][09]               |   ✔️    |   ✔️    |
| 10  | [Cathode-Ray Tube][10]          |   ❌    |   ❌    |
| 11  | [Monkey in the Middle][11]      |   ❌    |   ❌    |
| 12  | [Hill Climbing Algorithm][12]   |   ❌    |   ❌    |
| 13  | [Distress Signal][13]           |   ❌    |   ❌    |
| 14  | [Regolith Reservoir][14]        |   ❌    |   ❌    |
| 15  | [Beacon Exclusion Zone][15]     |   ❌    |   ❌    |
| 16  | [Proboscidea Volcanium][16]     |   ❌    |   ❌    |
| 17  | [Pyroclastic Flow][17]          |   ❌    |   ❌    |
| 18  | [Boiling Boulders][18]          |   ❌    |   ❌    |
| 19  | [Not Enough Minerals][19]       |   ❌    |   ❌    |
| 20  | [Grove Positioning System][20]  |   ❌    |   ❌    |
| 21  | [Monkey Math][21]               |   ❌    |   ❌    |
| 22  | [Monkey Map][22]                |   ❌    |   ❌    |
| 23  | [Unstable Diffusion][23]        |   ❌    |   ❌    |
| 24  | [Blizzard Basin][24]            |   ❌    |   ❌    |
| 25  | [Full of Hot Air][25]           |   ❌    |   ❌    |

## Makefile

This `Makefile` is used to compile all codes, if there is an exception, a
custom `Makefile` will be available in the corresponding directory.

```Makefile
CC=gcc
CFLAGS=-g -Wall
OBJS=main.o
BIN=main

all: $(BIN)

main: $(OBJS)
        $(CC) $(CFLAGS) $(OBJS) -o main

%.o: %.c
        $(CC) $(CFLAGS) -c $< -o $@

clean:
        $(RM) -r main *.o *.c~

run: $(BIN)
        ./main
```

## Style

I *try* to follow the `Linux` style, I use the `indent` package to *fix* indentation.
Make sure your editor don't convert tabs to spaces, one tab equals to 8 spaces.

```shell
indent -linux -l100 main.c
```

[01]: https://adventofcode.com/2022/day/1
[02]: https://adventofcode.com/2022/day/2
[03]: https://adventofcode.com/2022/day/3
[04]: https://adventofcode.com/2022/day/4
[05]: https://adventofcode.com/2022/day/5
[06]: https://adventofcode.com/2022/day/6
[07]: https://adventofcode.com/2022/day/7
[08]: https://adventofcode.com/2022/day/8
[09]: https://adventofcode.com/2022/day/9
[10]: https://adventofcode.com/2022/day/10
[11]: https://adventofcode.com/2022/day/11
[12]: https://adventofcode.com/2022/day/12
[13]: https://adventofcode.com/2022/day/13
[14]: https://adventofcode.com/2022/day/14
[15]: https://adventofcode.com/2022/day/15
[16]: https://adventofcode.com/2022/day/16
[17]: https://adventofcode.com/2022/day/17
[18]: https://adventofcode.com/2022/day/18
[19]: https://adventofcode.com/2022/day/19
[20]: https://adventofcode.com/2022/day/20
[21]: https://adventofcode.com/2022/day/21
[22]: https://adventofcode.com/2022/day/22
[23]: https://adventofcode.com/2022/day/23
[24]: https://adventofcode.com/2022/day/24
[25]: https://adventofcode.com/2022/day/25
