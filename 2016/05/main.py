"""
Advent of Code 2016
--- Day 5: How About a Nice Game of Chess? ---
https://adventofcode.com/2016/day/5

Author: @hasecilu
"""

import hashlib


class DoorPasswordCraking:
    def __init__(self, door_id):
        self.door_id = door_id

    def solve_part1(self):
        """Brute force md5 checksums to get the password of the first door."""
        password = ""
        index = 0
        while True:
            s = self.door_id + str(index)
            md5hash = hashlib.md5(s.encode()).hexdigest()
            if md5hash[:5] == "00000":
                password += md5hash[5]
                print(f"\r{password}", end="")

            if len(password) == 8:
                print()
                return password
            else:
                index += 1

    def solve_part2(self):
        """Brute force md5 checksums to get the password of the second door."""
        password = [""] * 8
        index = 0
        while True:
            s = self.door_id + str(index)
            md5hash = hashlib.md5(s.encode()).hexdigest()
            if (
                md5hash[:5] == "00000"
                and md5hash[5] in "01234567"
                and password[int(md5hash[5])] == ""
            ):
                password[int(md5hash[5])] = md5hash[6]
                print(f"\r{password}", end="")

            p = 0
            for i in range(8):
                if password[i] != "":
                    p += 1
            if p == 8:
                print()
                return "".join(password)
            else:
                index += 1


def test():
    assert DoorPasswordCraking("abc").solve_part1() == "18f47a30"
    assert DoorPasswordCraking("abc").solve_part2() == "05ace8e3"
    print("All test cases passed!")


if __name__ == "__main__":
    test()

    solver = DoorPasswordCraking("cxdnnyjw")
    print(f"The password of first door is {solver.solve_part1()}")
    print(f"The password of second door is {solver.solve_part2()}")
