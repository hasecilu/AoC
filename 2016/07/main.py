"""
Advent of Code 2016
--- Day 7: Internet Protocol Version 7 ---
https://adventofcode.com/2016/day/7

Author: @hasecilu
"""

import re


class IPSupport:
    def __init__(self, ip_addresses):
        self.ip_addresses = ip_addresses

    def has_abba(self, seq: str):
        abba = False
        for i in range(len(seq) - 3):
            if seq[i] != seq[i + 1] and seq[i] == seq[i + 3] and seq[i + 1] == seq[i + 2]:
                abba = True
        return abba

    def solve_part1(self):
        """Get number of IPs supporting TLC."""
        ip_with_tls_support = 0
        for ip_address in self.ip_addresses:
            sequence = re.split(r"[\[\]]", ip_address)
            s = 0
            abbas = 0
            preliminar_tls_support = True
            for seq in sequence:
                if s % 2 == 0 and self.has_abba(seq):
                    abbas += 1
                if s % 2 == 1 and self.has_abba(seq):
                    preliminar_tls_support = False
                    break
                s += 1
            if preliminar_tls_support and abbas > 0:
                ip_with_tls_support += 1

        return ip_with_tls_support

    def get_ab(self, seq: str, aba_or_bab: str):
        abas = []
        for i in range(len(seq) - 2):
            if seq[i] != seq[i + 1] and seq[i] == seq[i + 2]:
                abas.append(seq[i : i + 2])
        if abas == []:
            return []
        if aba_or_bab == "ABA":
            return abas
        elif aba_or_bab == "BAB":
            return [s[::-1] for s in abas]

    def solve_part2(self):
        """Get number of IPs supporting SSL."""
        ip_with_ssl_support = 0
        for ip_address in self.ip_addresses:
            supernet_seq = []
            hypernet_seq = []
            sequence = re.split(r"[\[\]]", ip_address)
            s = 0
            for seq in sequence:
                if s % 2 == 0:
                    supernet_seq.extend(
                        [item for item in self.get_ab(seq, "ABA") if item is not None]
                    )
                else:
                    hypernet_seq.extend(
                        [item for item in self.get_ab(seq, "BAB") if item is not None]
                    )
                s += 1
            common_elements = set(supernet_seq) & set(hypernet_seq)
            if len(common_elements) > 0:
                ip_with_ssl_support += 1

        return ip_with_ssl_support


def test():
    ips = ["abba[mnop]qrst", "abcd[bddb]xyyx", "aaaa[qwer]tyui", "ioxxoj[asdfgh]zxcvbn"]
    assert IPSupport(ips).solve_part1() == 2
    ips = ["aba[bab]xyz", "xyx[xyx]xyx", "aaa[kek]eke", "zazbz[bzb]cdb"]
    assert IPSupport(ips).solve_part2() == 3
    print("All test cases passed!")


if __name__ == "__main__":
    test()

    with open("input", "r") as file:
        input = file.readlines()

    solver = IPSupport(input)
    print(f"{solver.solve_part1()} IPs support TLS.")
    print(f"{solver.solve_part2()} IPs support SSL.")
