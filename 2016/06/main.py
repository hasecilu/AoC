"""
Advent of Code 2016
--- Day 6: Signals and Noise ---
https://adventofcode.com/2016/day/6

Author: @hasecilu
"""

from collections import defaultdict


class DecodeMessages:
    def __init__(self, messages):
        self.messages = [m.strip() for m in messages]
        self.position_counts = [defaultdict(int) for _ in range(len(self.messages[0]))]

        for msg in self.messages:
            for i, char in enumerate(msg):
                self.position_counts[i][char] += 1

    def solve_part1(self):
        """Get message from most common letters."""
        most_frequent_letters = []
        for counts in self.position_counts:
            most_frequent_letter = max(counts, key=counts.get)
            most_frequent_letters.append(most_frequent_letter)

        return "".join(most_frequent_letters)

    def solve_part2(self):
        """Get message from least common letters."""
        least_frequent_letters = []
        for counts in self.position_counts:
            most_frequent_letter = min(counts, key=counts.get)
            least_frequent_letters.append(most_frequent_letter)

        return "".join(least_frequent_letters)


def test():
    messages = [
        "eedadn",
        "drvtee",
        "eandsr",
        "raavrd",
        "atevrs",
        "tsrnev",
        "sdttsa",
        "rasrtv",
        "nssdts",
        "ntnada",
        "svetve",
        "tesnvt",
        "vntsnd",
        "vrdear",
        "dvrsen",
        "enarar",
    ]
    assert DecodeMessages(messages).solve_part1() == "easter"
    assert DecodeMessages(messages).solve_part2() == "advent"
    print("All test cases passed!")


if __name__ == "__main__":
    test()

    with open("input", "r") as file:
        input = file.readlines()

    solver = DecodeMessages(input)
    print(f"The error-corrected version of the message is <{solver.solve_part1()}>.")
    print(f"The error-corrected version of the message is <{solver.solve_part2()}>.")
