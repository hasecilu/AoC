"""
Advent of Code 2016
--- Day 3: Squares With Three Sides ---
https://adventofcode.com/2016/day/3

Author: @hasecilu
"""


class TriangleValidator:
    def __init__(self, triplets):
        self.triplets = triplets

    def solve_part1(self):
        """Determine how many horizontal triplets create a triangle."""
        valid_triangles = 0
        for triplet in self.triplets:
            numbers = triplet.split()
            numbers = [int(num) for num in numbers]
            if (
                numbers[0] + numbers[1] > numbers[2]
                and numbers[0] + numbers[2] > numbers[1]
                and numbers[1] + numbers[2] > numbers[0]
            ):
                valid_triangles += 1
        return valid_triangles

    def solve_part2(self):
        """Determine how many vertical triplets create a triangle."""
        valid_triangles = 0
        for i in range(0, len(self.triplets), 3):
            numpack = self.triplets[i : i + 3]
            numbers = [[], [], []]
            for i in range(3):
                numbers[i] = numpack[i].split()
                numbers[i] = [int(num) for num in numbers[i]]
            for i in range(3):
                if (
                    numbers[0][i] + numbers[1][i] > numbers[2][i]
                    and numbers[0][i] + numbers[2][i] > numbers[1][i]
                    and numbers[1][i] + numbers[2][i] > numbers[0][i]
                ):
                    valid_triangles += 1
        return valid_triangles


if __name__ == "__main__":
    with open("input", "r") as file:
        input = file.readlines()

    solver = TriangleValidator(input)
    print(f"There are {solver.solve_part1()} valid triangles.")
    print(f"There are {solver.solve_part2()} valid triangles.")
