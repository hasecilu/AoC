"""
Advent of Code 2016
--- Day 1: No Time for a Taxicab ---
https://adventofcode.com/2016/day/1

Author: @hasecilu
"""


class BlocksCounter:
    cardinal_directions = ("N", "E", "S", "W")

    def __init__(self, instructions):
        self.instructions = instructions
        self.dir = 0
        self.position = [0, 0]
        self.visited = []

    def solve_part1(self):
        """Return the distance from origin to final position."""
        tokens = self.instructions.split(", ")
        for token in tokens:
            turn = token[0]
            steps = int(token[1:])
            # update direction
            if turn == "R":
                self.dir += 1
            elif turn == "L":
                # avoid enter on negative domain because mod() is a par function
                if self.dir == 0:
                    self.dir += 4
                self.dir -= 1  # equivalent to sum 3
            # move n steps
            direction = self.cardinal_directions[self.dir % 4]
            if direction == "N":
                for _ in range(steps):
                    self.position[0] += 1
                    self.visited.append(tuple(self.position))
                    # print(f"({self.position[0]}, {self.position[1]})")
            elif direction == "E":
                for _ in range(steps):
                    self.position[1] += 1
                    self.visited.append(tuple(self.position))
                    # print(f"({self.position[0]}, {self.position[1]})")
            elif direction == "S":
                for _ in range(steps):
                    self.position[0] -= 1
                    self.visited.append(tuple(self.position))
                    # print(f"({self.position[0]}, {self.position[1]})")
            elif direction == "W":
                for _ in range(steps):
                    self.position[1] -= 1
                    self.visited.append(tuple(self.position))
                    # print(f"({self.position[0]}, {self.position[1]})")

        print(f"Final position ({self.position[0]}, {self.position[1]})\n")
        return abs(self.position[0]) + abs(self.position[1])

    def solve_part2(self):
        """Return the distance from origin to first duplicated coordinate."""
        seen = set()
        for coord in self.visited:
            if coord in seen:
                return abs(coord[0]) + abs(coord[1])
            seen.add(coord)
        return None  # In case no repetition is found


def test():
    assert BlocksCounter("R2, L3").solve_part1() == 5
    assert BlocksCounter("R2, R2, R2").solve_part1() == 2
    assert BlocksCounter("R5, L5, R5, R3").solve_part1() == 12
    case4 = BlocksCounter("R8, R4, R4, R8")
    assert case4.solve_part1() == 8
    assert case4.solve_part2() == 4
    print("All test cases passed!")


if __name__ == "__main__":
    test()

    with open("input", "r") as file:
        file_content = file.read()

    solver = BlocksCounter(file_content)
    print(f"Easter Bunny HQ is {solver.solve_part1()} blocks away")
    print(f"First repeated location is {solver.solve_part2()} blocks away")
