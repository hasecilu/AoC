"""
Advent of Code 2016
--- Day 2: Bathroom Security ---
https://adventofcode.com/2016/day/2

Author: @hasecilu
"""


class BathroomCodeCracker:
    def __init__(self, instructions):
        self.instructions = instructions

    def solve_part1(self):
        """Decipher the code for a normal keypad."""
        keypad = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        position = [1, 1]
        code = []
        for line in self.instructions:
            for char in line:
                if char == "U":
                    position[0] = max(position[0] - 1, 0)
                elif char == "D":
                    position[0] = min(position[0] + 1, 2)
                elif char == "L":
                    position[1] = max(position[1] - 1, 0)
                elif char == "R":
                    position[1] = min(position[1] + 1, 2)

            code.append(keypad[position[0]][position[1]])
        return int("".join(map(str, code)))

    def solve_part2(self):
        """Decipher the code for the over-engineered keypad."""
        keypad = [
            ["", "", "1", "", ""],
            ["", "2", "3", "4", ""],
            ["5", "6", "7", "8", "9"],
            ["", "A", "B", "C", ""],
            ["", "", "D", "", ""],
        ]
        position = [2, 0]
        code = []
        for line in self.instructions:
            for char in line:
                if char == "U" and position[0] >= 1 and keypad[position[0] - 1][position[1]] != "":
                    position[0] = max(position[0] - 1, 0)
                elif (
                    char == "D" and position[0] <= 3 and keypad[position[0] + 1][position[1]] != ""
                ):
                    position[0] = min(position[0] + 1, 4)
                elif (
                    char == "L" and position[1] >= 1 and keypad[position[0]][position[1] - 1] != ""
                ):
                    position[1] = max(position[1] - 1, 0)
                elif (
                    char == "R" and position[1] <= 3 and keypad[position[0]][position[1] + 1] != ""
                ):
                    position[1] = min(position[1] + 1, 4)

            code.append(keypad[position[0]][position[1]])
        return "".join(map(str, code))


def test():
    s = ["ULL", "RRDDD", "LURDL", "UUUUD"]
    assert BathroomCodeCracker(s).solve_part1() == 1985
    assert BathroomCodeCracker(s).solve_part2() == "5DB3"
    print("All test cases passed!")


if __name__ == "__main__":
    test()

    with open("input", "r") as file:
        input = file.readlines()

    solver = BathroomCodeCracker(input)
    print(f"The code is <{solver.solve_part1()}>")
    print(f"The code is <{solver.solve_part2()}>")
