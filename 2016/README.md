# Advent of Code 2016

| Day | Problem                                      | Part 1 | Part 2 |
| :-: | :------------------------------------------- | :----: | :----: |
| 01  | [No Time for a Taxicab][01]                  |   ✔️    |   ✔️    |
| 02  | [Bathroom Security][02]                      |   ✔️    |   ✔️    |
| 03  | [Squares With Three Sides][03]               |   ✔️    |   ✔️    |
| 04  | [Security Through Obscurity][04]             |   ✔️    |   ✔️    |
| 05  | [How About a Nice Game of Chess?][05]        |   ✔️    |   ✔️    |
| 06  | [Signals and Noise][06]                      |   ✔️    |   ✔️    |
| 07  | [Internet Protocol Versio ffn 7][07]         |   ✔️    |   ✔️    |
| 08  | [Two-Factor Authentication][08]              |   ✔️    |   ✔️    |
| 09  | [Explosives in Cyberspace][09]               |   ✔️    |   ✔️    |
| 10  | [Balance Bots][10]                           |   ✔️    |   ✔️    |
| 11  | [Radioisotope Thermoelectric Generators][11] |   ❌    |   ❌    |
| 12  | [Leonardo's Monorail][12]                    |   ❌    |   ❌    |
| 13  | [A Maze of Twisty Little Cubicles][13]       |   ❌    |   ❌    |
| 14  | [One-Time Pad][14]                           |   ❌    |   ❌    |
| 15  | [Timing is Everything][15]                   |   ❌    |   ❌    |
| 16  | [Dragon Checksum][16]                        |   ❌    |   ❌    |
| 17  | [Two Steps Forward][17]                      |   ❌    |   ❌    |
| 18  | [Like a Rogue][18]                           |   ❌    |   ❌    |
| 19  | [An Elephant Named Joseph][19]               |   ❌    |   ❌    |
| 20  | [Firewall Rules][20]                         |   ❌    |   ❌    |
| 21  | [Scrambled Letters and Hash][21]             |   ❌    |   ❌    |
| 22  | [Grid Computing][22]                         |   ❌    |   ❌    |
| 23  | [Safe Cracking][23]                          |   ❌    |   ❌    |
| 24  | [Air Duct Spelunking][24]                    |   ❌    |   ❌    |
| 25  | [Clock Signal][25]                           |   ❌    |   ❌    |

## Running the scripts

Just need to pass the `main.py` file to Python shell.

```shell
python3 main.py
```

## Style

I *try* to follow the `PEP 8` style guide with the exception of using lines of
100 char length.There is a `.ruff.toml` config file on root directory to let
LazyVim's Python extra specification (using Ruff as LSP) manage formatting.

[01]: https://adventofcode.com/2016/day/1
[02]: https://adventofcode.com/2016/day/2
[03]: https://adventofcode.com/2016/day/3
[04]: https://adventofcode.com/2016/day/4
[05]: https://adventofcode.com/2016/day/5
[06]: https://adventofcode.com/2016/day/6
[07]: https://adventofcode.com/2016/day/7
[08]: https://adventofcode.com/2016/day/8
[09]: https://adventofcode.com/2016/day/9
[10]: https://adventofcode.com/2016/day/10
[11]: https://adventofcode.com/2016/day/11
[12]: https://adventofcode.com/2016/day/12
[13]: https://adventofcode.com/2016/day/13
[14]: https://adventofcode.com/2016/day/14
[15]: https://adventofcode.com/2016/day/15
[16]: https://adventofcode.com/2016/day/16
[17]: https://adventofcode.com/2016/day/17
[18]: https://adventofcode.com/2016/day/18
[19]: https://adventofcode.com/2016/day/19
[20]: https://adventofcode.com/2016/day/20
[21]: https://adventofcode.com/2016/day/21
[22]: https://adventofcode.com/2016/day/22
[23]: https://adventofcode.com/2016/day/23
[24]: https://adventofcode.com/2016/day/24
[25]: https://adventofcode.com/2016/day/25
