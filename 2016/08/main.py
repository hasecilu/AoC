"""
Advent of Code 2016
--- Day 8: Two-Factor Authentication ---
https://adventofcode.com/2016/day/8

Author: @hasecilu
"""


class Screen:
    def __init__(self, instructions: list, x: int = 50, y: int = 6):
        self.instructions = instructions
        self.x = x
        self.y = y
        self.screen = [[0 for _ in range(x)] for _ in range(y)]  # x [0, 49]; y [0,5]

    def display_screen(self):
        """Print the alleged content of the screen."""
        for col in self.screen:
            print(" ".join("#" if row else "·" for row in col))
        print()

    def rect(self, size: list):
        """Set the pixels of a rectangle on the top-left corner."""
        for y in range(size[1]):  # [0,5]
            for x in range(size[0]):  # [0,49]
                self.screen[y][x] = 1

    def rotate(self, rotating_line: str, n: int):
        """Rotate the pixels of a specific line n times."""
        s = rotating_line.split("=")
        line = int(s[-1])
        if s[0] == "x":  # column
            for _ in range(n):
                last = self.screen[-1][line]
                for y in reversed(range(self.y)):
                    self.screen[y][line] = self.screen[(y - 1) % self.y][line]
                self.screen[0][line] = last
        elif s[0] == "y":  # row
            for _ in range(n):
                last = self.screen[line][-1]
                for x in reversed(range(self.x)):
                    self.screen[line][x] = self.screen[line][(x - 1) % self.x]
                self.screen[line][0] = last

    def solve_part1(self):
        """Get the number of lit pixels."""
        for instruction in self.instructions:
            tokens = instruction.strip().split()
            if len(tokens) == 2:
                size = list(map(int, tokens[1].split("x")))
                self.rect(size)
            else:
                self.rotate(tokens[2], int(tokens[-1]))
        self.display_screen()
        total_sum = sum(value for row in self.screen for value in row)
        return total_sum


def test():
    instructions = [
        "rect 3x2",
        "rotate column x=1 by 1",
        "rotate row y=0 by 4",
        "rotate column x=1 by 1",
    ]
    Screen(instructions, 7, 3).solve_part1()  # visual inspection


if __name__ == "__main__":
    test()

    with open("input", "r") as file:
        input = file.readlines()

    solver = Screen(input)
    print(f"{solver.solve_part1()} pixels should be lit.")
