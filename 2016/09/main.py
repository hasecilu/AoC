"""
Advent of Code 2016
--- Day 9: Explosives in Cyberspace ---
https://adventofcode.com/2016/day/9

Author: @hasecilu
"""


class Decompressor:
    def __init__(self, compressed_data):
        self.compressed_data = compressed_data

    def length_calculator(self, string: str) -> int:
        """Calculate the number characters of decompressed data recursively."""
        length = 0
        if "(" not in string and ")" not in string:
            return len(string)
        else:
            i0 = string.find("(")
            i1 = string.find(")")
            subsequent_characters, repetitions = map(int, string[i0 + 1 : i1].split("x"))

            # Since potential decompression instructions are discarded no need to expand the data
            length += i0 + subsequent_characters * repetitions
            new_string = string[i1 + 1 + subsequent_characters :]
            length += self.length_calculator(new_string)
            return length

    def solve_part1(self) -> int:
        """Get length of decompressed data for compression method v1."""
        return self.length_calculator(self.compressed_data)

    def instructions_solver(self, string: str, subsq_ch: int, acc_product: int) -> tuple:
        """
        Selects the adequate method to count the length of data depending on instructions' placement.

        This function determines if there are consecutive decompression instructions, if so, determines
        the length of the string applying multiplications.

        When instructions are not consecutive decompress the data and start again the search for new
        consecutive instructions in the substrings.

        Decompression instructions have the form: "(<subsequent_characters>x<repetitions>)"
        Consecutive instructions follow a expression to avoid expansion:
            decompressed_len("(27x12)(20x12)(13x14)(7x10)(1x12)A") = 12*12*14*10*12*1 = 241920

        Parameters:
        - string (str): The input string containing instructions. It may include nested instructions
            enclosed in parentheses and separated by the character 'x' to indicate repetition.
        - subsq_ch (int): The number of subsequent characters to repeat.
        - acc_product (int): The accumulated product of previous instructions.

        Returns:
        - The remaining string after removing the applied instructions.
        - The first element is the number of subsequent characters to repeat.
        - The second element is the accumulated product of previous consecutive instructions.
        """

        if string == "":
            # finished string
            return string, subsq_ch, subsq_ch * acc_product  # chars * reps
        elif string[0] != "(":
            # broken chain
            return string, subsq_ch, subsq_ch * acc_product  # chars * reps
        else:
            # check if instructions are consecutive or not and proceed according to that
            idx = [i for i, char in enumerate(string) if char == ")"]
            # if there are consecutive instructions after every ")" there is a "(" (except for the last)
            consecutive_instructions = True
            for i in idx[:-1]:
                if string[i + 1] != "(":
                    consecutive_instructions = False
                    break

            i0 = string.find("(")
            i1 = string.find(")")
            subsequent_characters, repetitions = map(int, string[i0 + 1 : i1].split("x"))

            if consecutive_instructions:
                # chain of consecutive decompression instructions continue
                # instead of expanding the string we can calculate the length arithmetically
                new_string = string[i1 + 1 :]
                return self.instructions_solver(
                    new_string, subsequent_characters, acc_product * repetitions
                )
            else:
                # since the instructions are not consecutive it's needed to get the length of
                # the string being expanded
                data_to_expand = string[i1 + 1 : i1 + 1 + subsequent_characters]
                remaining_data = string[i1 + 1 + subsequent_characters :]
                length = repetitions * self.length_calculator2(data_to_expand)
                return self.instructions_solver(remaining_data, 1, length)

    def length_calculator2(self, string: str) -> int:
        """Calculate the characters number of decompressed data recursively accounting for nested instructions."""
        length = 0
        if "(" not in string and ")" not in string:
            # no more decompression instructions
            return len(string)
        else:
            i0 = string.find("(")
            i1 = string.find(")")
            length += i0
            subsequent_characters, _ = map(int, string[i0 + 1 : i1].split("x"))
            substring_for_decompression = string[i0 : i1 + 1 + subsequent_characters]
            remaining_data = string[i1 + 1 + subsequent_characters :]

            # NOTE: The new specification allows nested instructions, so we need to expand them
            # All decompression instructions will be deleted remaining only data
            string_wo_di, subsq_ch, acc_product = self.instructions_solver(
                substring_for_decompression, 1, 1
            )
            length += acc_product
            new_string = string_wo_di[subsq_ch:] + remaining_data
            length += self.length_calculator2(new_string)
            return length

    def solve_part2(self) -> int:
        """Get length of decompressed data for compression method v2."""
        return self.length_calculator2(self.compressed_data)


def test():
    assert Decompressor("ADVENT").solve_part1() == 6
    assert Decompressor("A(1x5)BC").solve_part1() == 7
    assert Decompressor("(3x3)XYZ").solve_part1() == 9
    assert Decompressor("A(2x2)BCD(2x2)EFG").solve_part1() == 11
    assert Decompressor("(6x1)(1x3)A").solve_part1() == 6
    assert Decompressor("X(8x2)(3x3)ABCY").solve_part1() == 18
    assert Decompressor("(3x3)XYZ").solve_part2() == 9
    assert Decompressor("X(8x2)(3x3)ABCY").solve_part2() == 20
    assert Decompressor("(27x12)(20x12)(13x14)(7x10)(1x12)A").solve_part2() == 241920
    assert (
        Decompressor("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN").solve_part2()
        == 445
    )
    print("All test cases passed!")


if __name__ == "__main__":
    test()

    with open("input", "r") as file:
        input = file.readline()

    solver = Decompressor(input.strip())
    print(f"The decompressed length of the file is {solver.solve_part1()}.")
    print(f"The decompressed length of the file is {solver.solve_part2()}.")
