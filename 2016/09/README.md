# --- Day 9: Explosives in Cyberspace ---

## Part 1

```mermaid
graph TD
    A[solve_part1] -- self.compressed_data --> B[length_calculator]
    B --> C{Does the string\nhave parentheses?}
    C -- No --> F[Return length]
    C -- Yes --> G[Find open/close parenthesis]
    G --> H[Parse decompress instruction]
    H --> I[Sum the length of string\nto length variable]
    I --> J[Get the new substring]
    J -- new_string --> B
```

## Part 2

```mermaid
graph TD
    A[solve_part2] -- self.compressed_data --> B[length_calculator2]
    B --> C{Does the\nstring have\ndecompression\ninstructions?}
    C -- No --> F[Return length]
    C -- Yes --> G[Find open/close parenthesis]
    G --> H[Parse decompress instruction]
    H -- substring_for_decompression, 1, 1 --> I[Instructions solver]
    I --> J{Is string empty\nor first char of\nstring is an opening\nparanthesis}
    J -- Yes --> K[return string, subsq_ch, subsq_ch * acc_product]
   L --> M{Are all\ninstructions\nconsecutive?}
   M -- No --> Ñ[Separate string in:\ndata to expand\nremaining data] 
   M -- Yes --> N[Remove decompression\ninstruction from\nmain string]
   N -- new_string,\nsubsequent_chars,\nacc_product * repetitions--> I
   J -- No --> L[Parse first instruction.\nGet subsequent characters\nand repetitions]
   Ñ --> O[Get length of data to expand\nlength=repetitions * length_calculator2]
   O -- remaining_data,\n1,\nlength --> I
```
