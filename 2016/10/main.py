"""
Advent of Code 2016
--- Day 10: Balance Bots ---
https://adventofcode.com/2016/day/10

Author: @hasecilu
"""

from dataclasses import dataclass, field
from typing import Dict, List


@dataclass
class BotData:
    chips: List[int] = field(default_factory=list)
    lower_num: int = 0
    higher_num: int = 0
    lower_type: str = ""
    higher_type: str = ""


class Robots:
    """Manage the behaviour of the factory."""

    def __init__(self, instructions: list[str], chips_to_compare: list[int]):
        self.instructions = instructions
        self.instructions.sort()  # first prepare connections then make chips flow
        self.bot_graph: Dict[int, BotData] = {}
        self.output_bins: Dict[int, int] = {}
        self.MAX = 2
        self.chips_to_compare = chips_to_compare
        self.responsible_bot = -1

    def print_factory_status(self, full_data: bool = False):
        for bot, bot_data in self.bot_graph.items():
            if full_data:
                print(f"bot: {bot}, data: {bot_data}")
            else:
                print(f"bot: {bot}, data: {bot_data.chips}")
        for out, data in self.output_bins.items():
            print(f"output bin: {out}, data: {data}")

    def give_microchips(self, giving_bot: int):
        """Moves the 2 chips from a bot to other 2 bots"""
        self.chips_to_compare.sort()
        self.bot_graph[giving_bot].chips.sort()
        if self.chips_to_compare == self.bot_graph[giving_bot].chips:
            self.responsible_bot = giving_bot

        receiving_min_bot = self.bot_graph[giving_bot].lower_num
        receiving_max_bot = self.bot_graph[giving_bot].higher_num
        min_val = min(self.bot_graph[giving_bot].chips)
        max_val = max(self.bot_graph[giving_bot].chips)

        if self.bot_graph[giving_bot].lower_type == "bot":
            self.bot_graph[receiving_min_bot].chips.append(min_val)
            if len(self.bot_graph[receiving_min_bot].chips) == self.MAX:
                self.give_microchips(receiving_min_bot)
        else:  # output_bin
            self.output_bins[receiving_min_bot] = min_val

        if self.bot_graph[giving_bot].higher_type == "bot":
            self.bot_graph[receiving_max_bot].chips.append(max_val)
            if len(self.bot_graph[receiving_max_bot].chips) == self.MAX:
                self.give_microchips(receiving_max_bot)
        else:  # output_bin
            self.output_bins[receiving_max_bot] = max_val

        self.bot_graph[giving_bot].chips = []

    def solve_part1(self):
        """Returns the number of the responsible_bot for the indicated pair."""
        for instruction in self.instructions:
            tokens = instruction.strip().split()
            if len(tokens) == 12:
                bot_number = int(tokens[1])
                low = int(tokens[6])
                high = int(tokens[11])

                # Ensure the bot exists in the dictionary
                if bot_number not in self.bot_graph:
                    self.bot_graph[bot_number] = BotData()

                self.bot_graph[bot_number].lower_num = low
                self.bot_graph[bot_number].higher_num = high
                self.bot_graph[bot_number].lower_type = tokens[5]
                self.bot_graph[bot_number].higher_type = tokens[10]
            else:  # value
                value = int(tokens[1])
                bot = int(tokens[5])

                if len(self.bot_graph[bot].chips) == 0:
                    self.bot_graph[bot].chips.append(value)
                elif len(self.bot_graph[bot].chips) == 1:
                    self.bot_graph[bot].chips.append(value)
                    self.give_microchips(bot)  # get rid of 2 chips
        # self.print_factory_status()
        return self.responsible_bot


def test():
    instructions = [
        "value 5 goes to bot 2",
        "bot 2 gives low to bot 1 and high to bot 0",
        "value 3 goes to bot 1",
        "bot 1 gives low to output 1 and high to bot 0",
        "bot 0 gives low to output 2 and high to output 0",
        "value 2 goes to bot 2",
    ]

    d = Robots(instructions, [5, 2])
    assert d.solve_part1() == 2
    print("All test cases passed!")


if __name__ == "__main__":
    test()

    with open("input", "r") as file:
        input = file.readlines()

    solver = Robots(input, [61, 17])
    print(
        f"The number of the bot that is responsible for comparing "
        f"value-{solver.chips_to_compare[0]} microchips with "
        f"value-{solver.chips_to_compare[1]} microchips is {solver.solve_part1()}."
    )
    print(
        f"Multiplying the values of output_bins from 0 to 2 gives: "
        f"{solver.output_bins[0]} * {solver.output_bins[1]} * {solver.output_bins[2]} = "
        f"{solver.output_bins[0]*solver.output_bins[1]*solver.output_bins[2]}"
    )
