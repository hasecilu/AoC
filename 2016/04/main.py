"""
Advent of Code 2016
--- Day 4: Security Through Obscurity ---
https://adventofcode.com/2016/day/4

Author: @hasecilu
"""

import re
from collections import Counter


class RoomNameDecrypter:
    def __init__(self, raw_rooms):
        self.raw_rooms = raw_rooms
        self.real_rooms = []

    def calc_checksum(self, room: str):
        """Calculate the checksum for a encrypted room."""
        char_count = Counter(room)
        # Sort items: first by count (descending), then alphabetically (ascending)
        sorted_char_count = list(sorted(char_count.items(), key=lambda item: (-item[1], item[0])))
        cs = ""
        for i in range(5):
            cs += sorted_char_count[i][0]  # keys -> letters
        return cs

    def solve_part1(self):
        """Find the real rooms."""
        sum = 0
        for room in self.raw_rooms:
            # Extract checksum
            room = re.sub("\n", "", room)
            checksum_pattern = r"\[(.*?)\]"
            matches = re.findall(checksum_pattern, room)
            result = re.sub(checksum_pattern, "", room)
            checksum = matches[0]
            # Separate encrypted room from sector ID
            parts = result.rsplit("-", 1)
            encrypted_room = parts[0]
            sector_id = parts[-1]

            # print(f"enc room: {encrypted_room}\nID: {sector_id}\nchecksum: {checksum}\n")
            if self.calc_checksum(re.sub("-", "", encrypted_room)) == checksum:
                self.real_rooms.append((encrypted_room, sector_id))
                sum += int(sector_id)
        return sum

    def get_real_name(self, room_name: str, id: int):
        """Decrypt room name."""
        real_name = ""
        for ch in room_name:
            if ch == "-":
                c = " "
            else:
                c = ch
                for _ in range(int(id)):
                    if c == "z":
                        c = "a"
                    else:
                        c = chr(ord(c) + 1)
            real_name += c
        return real_name

    def solve_part2(self):
        """Find room where North Pole objects are stored."""
        for room_name, id in self.real_rooms:
            real_name = self.get_real_name(room_name, id)
            if re.findall(r"north", real_name) != []:
                print(f'Room name: "{real_name}"\tID: {id}')


def test():
    rooms = [
        "aaaaa-bbb-z-y-x-123[abxyz]",
        "a-b-c-d-e-f-g-h-987[abcde]",
        "not-a-real-room-404[oarel]",
        "totally-real-room-200[decoy]",
    ]
    assert RoomNameDecrypter(rooms).solve_part1() == 1514
    assert (
        RoomNameDecrypter(rooms).get_real_name("qzmt-zixmtkozy-ivhz", 343) == "very encrypted name"
    )
    print("All test cases passed!")


if __name__ == "__main__":
    test()

    with open("input", "r") as file:
        input = file.readlines()

    solver = RoomNameDecrypter(input)
    print(f"The sum of the sector IDs of the real rooms is {solver.solve_part1()}.")
    solver.solve_part2()
