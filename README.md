# Advent of Code

Some codes solving problems of [AoC](https://adventofcode.com/).

|      Year      | Language |
| :------------: | :------: |
| [2015](./2015) |    C     |
| [2016](./2016) |  Python  |
| [2022](./2022) |    C     |